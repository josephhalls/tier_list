import * as InAppPurchases from 'expo-in-app-purchases';
import Config from './Config';

export default CheckPremium = async () => {  

    const history = await InAppPurchases.connectAsync();

    if (history.responseCode === InAppPurchases.IAPResponseCode.OK) {
        history.results.forEach(result => {
            if(result.acknowledged){
                Config.isPremium = true;
            }
        });
    }
}