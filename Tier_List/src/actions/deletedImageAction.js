import {DELETED_IMAGE} from '../types';

export const deletedImageAction = (arr) => {
    return {
        type: DELETED_IMAGE,
        payload: arr
    };
};