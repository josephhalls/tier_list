import {
    SECTION_WIDTH,
    TOP_OFFSET,
    ROW_HEIGHT,
    IMAGE_HEIGHT,
    IMAGE_WIDTH,
    PLUS_WIDTH,
    LEFT_ARROW_WIDTH,
    SCROLL_OFFSET,
    SELECTED_IMAGE,
    MAIN_DROP_AREA_HEIGHT,
    IMAGE_DROP_SECTION_HEIGHT,
    IMAGE_DROP_SECTION_WIDTH
} from '../types';

export const sectionWidthAction = (bool) => {
    return {
        type: SECTION_WIDTH,
        payload: bool
    };
};

export const topOffsetAction = (bool) => {
    return {
        type: TOP_OFFSET,
        payload: bool
    };
};

export const rowHeightAction = (bool) => {
    return {
        type: ROW_HEIGHT,
        payload: bool
    };
};

export const imageHeightAction = (bool) => {
    return {
        type: IMAGE_HEIGHT,
        payload: bool
    };
};

export const imageWidthAction = (bool) => {
    return {
        type: IMAGE_WIDTH,
        payload: bool
    };
};

export const plusWidthAction = (bool) => {
    return {
        type: PLUS_WIDTH,
        payload: bool
    };
};

export const leftArrowWidthAction = (bool) => {
    return {
        type: LEFT_ARROW_WIDTH,
        payload: bool
    };
};

export const scrollOffsetAction = (bool) => {
    return {
        type: SCROLL_OFFSET,
        payload: bool
    };
};

export const selectedImageAction = (bool) => {
    return {
        type: SELECTED_IMAGE,
        payload: bool
    };
};

export const mainDropAreaHeightAction = (bool) => {
    return {
        type: MAIN_DROP_AREA_HEIGHT,
        payload: bool
    };
};

export const imageDropSectionHeightAction = (bool) => {
    return {
        type: IMAGE_DROP_SECTION_HEIGHT,
        payload: bool
    };
};

export const imageDropSectionWidthAction = (bool) => {
    return {
        type: IMAGE_DROP_SECTION_WIDTH,
        payload: bool
    };
};