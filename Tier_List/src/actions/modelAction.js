import {MODEL} from '../types';

export const modelAction = (obj) => {
    return {
        type: MODEL,
        payload: obj
    };
};