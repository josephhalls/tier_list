import {AUTO_PADDING} from '../types';

export const autoPaddingAction = (bool) => {
    return {
        type: AUTO_PADDING,
        payload: bool
    };
};