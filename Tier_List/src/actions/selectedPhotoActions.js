import {SELECTED_PHOTO, IS_PHOTO_SELECTED, SHOW_MODAL, SHOW_COLOUR_PICKER, MULTI_SELECT_OBJ} from '../types';

export const photoChangedAction = (object) => {
    return {
        type: SELECTED_PHOTO,
        payload: object
    };
};

export const hasPhotoChangedAction = (text) => {
    return {
        type: IS_PHOTO_SELECTED,
        payload: text
    };
};

export const showModalAction = (bool) => {
    return {
        type: SHOW_MODAL,
        payload: bool
    };
};

export const showMultiSelectAction = (obj) => {
    return {
        type: MULTI_SELECT_OBJ,
        payload: obj
    };
};

export const showColourPickerAction = (object) => {
    return {
        type: SHOW_COLOUR_PICKER,
        payload: object
    };
};