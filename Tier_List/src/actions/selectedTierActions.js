import {SELECTED_TIER} from '../types';

export const tierChangedAction = (id) => {
    return {
        type: SELECTED_TIER,
        payload: id
    };
};