import {MODEL} from '../types';

export const saveDraftAction = (obj) => {
    return {
        type: MODEL,
        payload: obj
    };
};