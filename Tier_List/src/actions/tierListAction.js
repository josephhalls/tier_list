import {TIER_LIST} from '../types';

export const tierListAction = (array) => {
    return {
        type: TIER_LIST,
        payload: array
    };
};