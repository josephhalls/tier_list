import {IMAGE_STYLE} from '../types';

export const imageStyleAction = (text) => {
    return {
        type: IMAGE_STYLE,
        payload: text
    };
};