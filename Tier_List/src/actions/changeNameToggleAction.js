import {CHANGE_NAME_TOGGLE} from '../types';

export const changeNameToggleAction = (bool) => {
    return {
        type: CHANGE_NAME_TOGGLE,
        payload: bool
    };
};