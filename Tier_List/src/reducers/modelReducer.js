import {MODEL} from '../types';

const INITIAL_STATE = {
    model: {}
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
    case MODEL:
        return{...state, model: action.payload};
    default:
        return state;
    }
};