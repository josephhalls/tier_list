import {AUTO_PADDING} from '../types';

const INITIAL_STATE = {
    autoPadding: true
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
    case AUTO_PADDING:
        return{...state, autoPadding: action.payload};
    default:
        return state;
    }
};