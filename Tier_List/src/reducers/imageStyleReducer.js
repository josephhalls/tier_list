import {IMAGE_STYLE} from '../types';

const INITIAL_STATE = {
    text: 'stretch'
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
    case IMAGE_STYLE:
        return{...state, imageStyle: action.payload};
    default:
        return state;
    }
}