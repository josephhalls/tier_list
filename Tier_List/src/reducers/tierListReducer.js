import {TIER_LIST} from '../types';

const INITIAL_STATE = {
    tierListArray: {list: []}
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
    case TIER_LIST:
        return{...state, tierListArray: action.payload};
    default:
        return state;
    }
}