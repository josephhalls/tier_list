import {SHOW_MODAL, SHOW_COLOUR_PICKER, MULTI_SELECT_OBJ} from '../types';

const INITIAL_STATE = {
    showModal: false,
    showColourPicker: {
        show: false, 
        type: ''
    },
    multiSelectObj: {
        count: 0,
        show: false
    }
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
    case SHOW_MODAL:
        return{...state, showModal: action.payload};
    case MULTI_SELECT_OBJ:
        return{...state, multiSelectObj: action.payload};
    case SHOW_COLOUR_PICKER:
        return{...state, showColourPicker: action.payload};
    default:
        return state;
    }
}