import {DELETED_IMAGE} from '../types';

const INITIAL_STATE = {
    deletedImage: []
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
    case DELETED_IMAGE:
        return{...state, deletedImage: action.payload};
    default:
        return state;
    }
};