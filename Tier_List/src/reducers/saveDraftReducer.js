import {SAVE_DRAFT} from '../types';

const INITIAL_STATE = {
    saveDraftArray: []
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
    case SAVE_DRAFT:
        return{...state, saveDraftArray: action.payload};
    default:
        return state;
    }
};