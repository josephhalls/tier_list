import {CHANGE_NAME_TOGGLE} from '../types';

const INITIAL_STATE = {
    changeNameToggle: false
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
    case CHANGE_NAME_TOGGLE:
        return{...state, changeNameToggle: action.payload};
    default:
        return state;
    }
};