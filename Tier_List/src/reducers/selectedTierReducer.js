import {SELECTED_TIER} from '../types';

const INITIAL_STATE = {
    selectedTier: 0
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
    case SELECTED_TIER:
        return{...state, selectedTier: action.payload};
    default:
        return state;
    }
}