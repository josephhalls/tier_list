import {
    SECTION_WIDTH,
    TOP_OFFSET,
    ROW_HEIGHT,
    IMAGE_HEIGHT,
    IMAGE_WIDTH,
    PLUS_WIDTH,
    LEFT_ARROW_WIDTH,
    SCROLL_OFFSET,
    SELECTED_IMAGE,
    MAIN_DROP_AREA_HEIGHT,
    IMAGE_DROP_SECTION_HEIGHT,
    IMAGE_DROP_SECTION_WIDTH
} from '../types';

const INITIAL_STATE = {
    sectionWidth: 0,
    topOffset: 0,
    rowHeight: 0,
    imageHeight: 0,
    imageWidth: 0,
    plusWidth: 0,
    leftArrowWidth: 0,
    scrollOffset: 0,
    selectedImage: {},
    mainDropAreaHeight: 0,
    imageDropSectionHeight: 0,
    imageDropSectionWidth: 0
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
    case SECTION_WIDTH:
        return{...state, sectionWidth: action.payload};
    case TOP_OFFSET:
        return{...state, topOffset: action.payload};
    case ROW_HEIGHT:
        return{...state, rowHeight: action.payload};
    case IMAGE_HEIGHT:
        return{...state, imageHeight: action.payload};
    case IMAGE_WIDTH:
        return{...state, imageWidth: action.payload};
    case PLUS_WIDTH:
        return{...state, plusWidth: action.payload};
    case LEFT_ARROW_WIDTH:
        return{...state, leftArrowWidth: action.payload};
    case SCROLL_OFFSET:
        return{...state, scrollOffset: action.payload};
    case SELECTED_IMAGE:
        return{...state, selectedImage: action.payload};
    case MAIN_DROP_AREA_HEIGHT:
        return{...state, mainDropAreaHeight: action.payload};
    case IMAGE_DROP_SECTION_HEIGHT:
        return{...state, imageDropSectionHeight: action.payload};
    case IMAGE_DROP_SECTION_WIDTH:
        return{...state, imageDropSectionWidth: action.payload};
    default:
        return state;
    }
};