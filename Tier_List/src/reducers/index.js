import {combineReducers} from 'redux';
import tierListReducer from './tierListReducer';
import selectedPhotoReducer from './selectedPhotoReducer';
import showModalReducer from './showModalReducer';
import selectedTierReducer from './selectedTierReducer';
import deletedImageReducer from './deletedImageReducer';
import saveDraftReducer from './saveDraftReducer';
import imageStyleReducer from './imageStyleReducer';
import autoPaddingReducer from './autoPaddingReducer';
import changeNameToggleReducer from './changeNameToggleReducer';
import dragDropReducer from './dragDropReducer';
import modelReducer from './modelReducer';

export default combineReducers({
    tierListReducer,
    selectedPhotoReducer,
    showModalReducer,
    selectedTierReducer,
    deletedImageReducer,
    imageStyleReducer,
    saveDraftReducer,
    autoPaddingReducer,
    changeNameToggleReducer,
    dragDropReducer,
    modelReducer,
});