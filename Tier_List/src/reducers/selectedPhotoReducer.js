import {SELECTED_PHOTO, IS_PHOTO_SELECTED} from '../types';

const INITIAL_STATE = {
    selectedPhoto: {
        uri: '',
        photoCount: 0
    },
    hasPhotoChanged: false
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
    case SELECTED_PHOTO:
        return{...state, selectedPhoto: action.payload};
    case IS_PHOTO_SELECTED:
        return{...state, hasPhotoChanged: action.payload};
    default:
        return state;
    }
}