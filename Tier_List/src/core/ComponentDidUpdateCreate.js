export const ComponentDidUpdateCreate = (self, prevProps) => {
    newImage(prevProps, self);
    checkNewColour(prevProps, self);
};

const newImage = (prevProps, self) => {
    if(prevProps.selectedPhoto !== self.props.selectedPhoto){
        let imageArr = self.state.images;
        const newImageArr = imageArr.concat(...self.props.selectedPhoto.images);
        self.setState({images: newImageArr});
    }
};

const checkNewColour = (prevProps, self) => {
    if(prevProps.selectedColour !== self.props.selectedColour){
        const {tierListArray, selectedColour} = self.props;
        const {editingRowId} = self.state;
        const colourVar = selectedColour.colour ? 'colour' :  'textColour';
        const index = editingRowId.index ? editingRowId.index : 0;
        if(!selectedColour[colourVar]){
            return;
        }
        tierListArray.list[index].section[colourVar] = selectedColour[colourVar];
        self.props.tierListAction(tierListArray);
    }
};