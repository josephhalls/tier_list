import * as ImagePicker from 'expo-image-picker';

export const requestCameraRollPermission = () => {
    return ImagePicker.getCameraRollPermissionsAsync()
        .then(hasUserGotPermission => {
            const permission = hasUserGotPermission.granted;
            return permission ? permission : requestPermission();
        });
};

const requestPermission = () => {
    return ImagePicker.requestCameraRollPermissionsAsync()
        .then( hasUserGotPermission => {
            return hasUserGotPermission.granted;
        });
};