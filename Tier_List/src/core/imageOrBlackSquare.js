import {Alert} from 'react-native';

export const imageOrBlackSquare = (self) => {

    if(self.props.autoPadding){
        self.addTierPressed();
    }
    else{
        Alert.alert(
            'Would you like to add a photo or black square?',
            '',
            [
                {
                    text: 'Photo',
                    onPress: () =>  {
                        self.addTierPressed();
                    }
                },
                {
                    text: 'Black Square',
                    onPress: () =>
                    {
                        self.addImage('Black');
                    }
                },
                {text: 'Cancel'}
            ],
            {cancelable: false},
        );
    }
};