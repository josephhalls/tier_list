import {Alert} from 'react-native';

export const ImagePressed = (image, self) => {
    return(
        Alert.alert(
            'Image pressed',
            'Would you like to edit this image?',
            [
                {
                    text: 'Delete Image',
                    onPress: () =>  deleteImage(image, self)
                },
                {
                    text: 'Change Image Style',
                    onPress: () =>  imageStyleModal(image, self)
                },
                {text: 'No'}
            ],
            {cancelable: false},
        )
    );
};

const imageStyleModal = (image, self) => {
    Alert.alert(
        'How do you want this image to look?',
        '',
        [
            {
                text: 'Stretch Style',
                onPress: () =>  {
                    updateImageStyle(image, 'stretch', self);
                }
            },
            {
                text: 'Cover Style',
                onPress: () =>
                {
                    updateImageStyle(image, 'cover', self);
                }
            },
            {
                text: 'Contain Style',
                onPress: () =>
                {
                    updateImageStyle(image, 'contain', self);
                }
            },
            {text: 'Cancel'}
        ],
        {cancelable: false},
    );
};

const updateImageStyle = (selectedImage, style, self) => {
    const {tierListArray} = self.props;
    const {images} = self.state;

    let newImageArray = images.map(image => {
        if(selectedImage === image){
            image.style = style;
        }
        return image;
    });

    let newTierListArray = tierListArray;
    newTierListArray.list = tierListArray.list.map(row => {
        const rowData = row;
        rowData.images = row.images.map(image => {
            if(selectedImage === image){
                image.style = style;
            }
            return image;
        });
        return rowData;
    });

    self.props.tierListAction(newTierListArray);
    self.setState({images: newImageArray});
};

const deleteImage = (image, self) => {
    Alert.alert(
        'Delete Image',
        'Would you like to delete this image?',
        [
            {
                text: 'Yes',
                onPress: () =>  findDeleteImage(image, self)
            },
            {text: 'No'}
        ],
        {cancelable: false},
    );
};

//Check which image array, could be bottom selectable ones
const findDeleteImage = (selectedImage, self) => {
    const {tierListArray} = self.props;
    const {images} = self.state;

    let newImageArray = images.filter(image => {
        if(selectedImage !== image){
            return image;
        }
    });

    let newTierListArray = tierListArray;
    newTierListArray.list = tierListArray.list.map(row => {
        const rowData = row;
        rowData.images = row.images.filter(image => {
            if(selectedImage !== image){
                return image;
            }
        });
        return rowData;
    });
    self.props.tierListAction(newTierListArray);
    self.setState({images: newImageArray});
};