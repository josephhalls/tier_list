import Config from '../../Config';

const CreateTierList = (temp, self) => {
    let tierListArr = [];
    const numberOfSections = temp ? 0 : Config.tierLevels;
    for (let i = 0; i < numberOfSections; i++) {
        const {sectionText, colour, fontColour} = getSectionHeaderLetterColour(i);
        tierListArr.push(self.standardObject(i, sectionText, colour, fontColour));
    }
    return {list: tierListArr};
};

const getSectionHeaderLetterColour = (index) => {
    const alphabetArr = ['S', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'],
        colourArr = ['#fc3c2d', '#ff5042', '#ff7e42', '#ffcd4f', '#5fe371', '#79d9b7', '#6db58e', '#6da9f7', '#2380fa', '#9563ff', '#7352ba'];

    let colourArrLength = colourArr.length;

    if (index >= colourArrLength) {
        const count = Math.floor(index / colourArrLength);
        index = index - colourArrLength * count;
    }

    return {sectionText: alphabetArr[index], colour: colourArr[index], fontColour: '#000000'};
};

export {CreateTierList, getSectionHeaderLetterColour};