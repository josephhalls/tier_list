import * as ImagePicker from 'expo-image-picker';
import {requestCameraRollPermission} from './requestCameraRollPermission';

export const androidImagePicker = () => {
    return requestCameraRollPermission().then(permission => {
        if(permission){
            return getImage();
        }
    });
};

const getImage = () => {
    return ImagePicker.launchImageLibraryAsync()
        .then(image => {
            if(image && !image.cancelled){
                return image.uri;
            }
        })
        .catch( e => {
            console.log(e);
        });
};