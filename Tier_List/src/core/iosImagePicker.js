import imagePicker from 'react-native-imagepicker';

export const iosImagePicker = () => {
    return imagePicker.open({
        takePhoto: true,
        useLastPhoto: false,
        chooseFromLibrary: true
    }).then(({ uri}) => {
        return uri;
    }, (error) => {
        // Typically, user cancel
        console.log('error', error);
    });
};