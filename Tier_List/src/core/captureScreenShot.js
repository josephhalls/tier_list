import { captureScreen } from 'react-native-view-shot';
import {Alert, CameraRoll} from 'react-native';

export const captureScreenShot = (cb) => {
    captureScreen({
        format: 'jpg',
        quality: 0.8
    })
        .then(uri => {
            CameraRoll.saveToCameraRoll(uri, 'photo')
                .then(() =>{
                    cb();
                    Alert.alert('Screenshot Downloaded to your device!');
                });
        },
        error => {cb(); console.error('Oops, snapshot failed', error)}
        )
        .catch(e => {
            Alert.alert(`Error ${e}`);
            cb();
        });
};