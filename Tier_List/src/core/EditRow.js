import {Alert} from 'react-native';

export const EditRow = (id, self) => {
    let index = 0;
    self.props.tierListArray.list.map((row, i) => {
        if(row.id === id){
            index = i;
        }
    });
    self.setState({editingRowId: {id, index}});
    Alert.alert(
        'Edit Tier Row',
        'Exit Edit Mode to apply your colour changes',
        [
            {
                text: 'Change Name',
                onPress: () => {
                    changeSectionName(self);
                }
            },
            {
                text: 'Change Background Colour',
                onPress: () => {
                    let colourCode = self.props.tierListArray.list[index].section.colour;
                    colourCode = colourCode ? colourCode : '#';
                    changeSectionColour({show: true, type: 'Background', colourCode}, self);
                }
            },
            {
                text: 'Change Font Colour',
                onPress: () => {
                    let colourCode = self.props.tierListArray.list[index].section.textColour;
                    colourCode = colourCode ? colourCode : '#';
                    changeSectionColour({show: true, type: 'Font', colourCode}, self);
                }
            },
            {
                text: 'Cancel',
            }
        ]
    );
};

const changeSectionName = (self) => {
    self.setState({showSectionDialogModal: true});
};

const changeSectionColour = (colourObject, self) => {
    self.props.showColourPickerAction(colourObject);
};