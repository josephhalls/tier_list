import {Alert} from 'react-native';

export const upgradeToPremium = () => {
    Alert.alert(
        'Premium Unlock',
        'Upgrade to Premium for this feature!',
        [
            {text: 'Back'}
        ],
        {cancelable: false},
    );
};