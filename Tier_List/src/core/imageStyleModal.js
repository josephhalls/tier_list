import {Alert} from 'react-native';

export const imageStyleModal = (self) => {
    Alert.alert(
        'How do you want your images to look?',
        '',
        [
            {
                text: 'Stretch Style',
                onPress: () =>  {
                    self.props.imageStyleAction('stretch');
                    self.setState({menuModal: false});
                }
            },
            {
                text: 'Cover Style',
                onPress: () =>
                {
                    self.props.imageStyleAction('cover');
                    self.setState({menuModal: false});
                }
            },
            {
                text: 'Contain Style',
                onPress: () =>
                {
                    self.props.imageStyleAction('contain');
                    self.setState({menuModal: false});
                }
            },
            {text: 'Cancel'}
        ],
        {cancelable: false},
    );
};