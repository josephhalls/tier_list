import React from 'react';
import {View} from 'react-native';
import {imageButton} from './imageButton';

export const mainHeader = (backBttn, menuObj = {}, headerFlex = 1) => {
    return (
        <View style={{flex: headerFlex, flexDirection: 'row', backgroundColor: 'black'}}>
            <View style={{flex: 0.2}}/>
            <View style={{flex: 1}}>
                <View style={{flex: 1}}/>
                {imageButton(require('../../assets/left_arrow.png'), backBttn)}
                <View style={{flex: 0.5}}/>
            </View>
            <View style={{flex: 4}}/>
            <View style={{flex: 1}}>
                <View style={{flex: 1}}/>
                {menuObj.show ? imageButton(require('../../assets/menu_icon.png'), menuObj.menuPressed) : <View />}
                <View style={{flex: 0.5}}/>
            </View>
            <View style={{flex: 0.5}}/>
        </View>
    );
};