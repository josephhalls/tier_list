export const SubmitInput = (inputText, self) => {
    const {tierListArray} = self.props;
    const {editingRowId} = self.state;
    const index = editingRowId.index ? editingRowId.index : 0;
    tierListArray.list[index].section.text = inputText;
    self.props.tierListAction(tierListArray);
    self.setState({showSectionDialogModal: false});
};