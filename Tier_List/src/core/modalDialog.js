import React from 'react';
import DialogInput from 'react-native-dialog-input';

export const modalDialog = (title, message, defaultText, self, submit, close) => {
    return (
        <DialogInput isDialogVisible={self.isDialogVisible}
            title={title}
            message={message}
            hintInput={''}
            initValueTextInput={defaultText}
            submitInput={(inputText) => {
                submit(inputText, self);
            }}
            closeDialog={() => {close();}}>
        </DialogInput>
    );
};