import React from 'react';
import {Animated, View, AsyncStorage, Alert} from 'react-native';
import {ImageCreator} from './ImageCreator';
import {RowCreator} from './RowCreator';
import {advertBanner} from './advertBanner';
import Config from '../../Config';
import CameraModalMultiSelect from '../components/common/CameraModalMultiSelect';
import {modalDialog} from './modalDialog';
import {SettingsModal} from './SettingsModal';
import {ImageScrollView} from './ImageScrollView';
import {ListButtons} from './ListButtons';
import {CreateTierList} from './CreateTierList';
import {retrieveData} from '../local_storage/retrieveData';
import {mainHeader} from './mainHeader';
import ColourPicker from '../components/common/ColourPicker';

export const RenderView = (self) => {
    const {dragging, draggingIdx, showDraggableImage, savingPhotos, DeviceOrientation} = self.state;
    const headerFlex = DeviceOrientation === 'PORTRAIT' ? 0.5 : 1;
    return(
        <View style={{flex: 1, backgroundColor: 'black'}}>
            {dragging && (<Animated.View
                style={{
                    width: '100%',
                    zIndex: 2,
                    position: 'absolute',
                    top: self.point.getLayout().top,
                    left: showDraggableImage ? self.point.getLayout().left : 0
                }}
            >
                {showDraggableImage ? ImageCreator(self.state.usableImage, self)
                    : RowCreator(self, getRowData(draggingIdx, self), -1, true)}
            </Animated.View>)}
            {advertBanner(Config, self)}
            {savingPhotos ? <View /> : mainHeader(self.backButton, {show: true, menuPressed: self.menuPressed}, headerFlex)}
            <View
                style={{flex: 5}}
                onLayout={e => {
                    self.model.topOffset = e.nativeEvent.layout.y;
                    self.model.mainDropAreaHeight = e.nativeEvent.layout.y + e.nativeEvent.layout.height;
                }}>
                {rows(self)}
            </View>
            <ColourPicker />
            <CameraModalMultiSelect />
            {self.state.showDraftModal ? modalDialog('Draft name', 'Please enter the draft name', '', self, submitDraftInput, self.closeDraftModal) : <View/>}
            {SettingsModal(self)}
            {bottomSection(self)}
            {rerenderView(self)}
        </View>
    );
};

const bottomSection = (self) => {
    const {DeviceOrientation, savingPhotos} = self.state;

    if(DeviceOrientation === 'PORTRAIT'){
        const flexVal = savingPhotos ? 0.5 : 1;
        return(
            <View style={{flex: flexVal}}>
                {ListButtons(self)}
                {ImageScrollView(self)}
            </View>
        );
    }
    const flexVal = savingPhotos ? 0.5 : 1.5;
    return(
        <View style={{flexDirection: 'row', flex: flexVal}}>
            {ListButtons(self)}
            {ImageScrollView(self)}
        </View>
    );
};

const rows = (self) => {
    const tierList = getRowData('all', self);

    return tierList.list.map((rowInfo, i) => {
        return RowCreator(self, rowInfo, i);
    });
};

const getRowData = (index, self) => {
    const listData = self.props.tierListArray;
    if(index === 'all'){
        return listData && listData.list[0] && listData.list[0].section ? listData : CreateTierList(false, self);
    }
    return listData.list[index];
};

const submitDraftInput = async (inputText, self) => {
    const {tierListArray} = self.props;
    let draftArr = await retrieveData();
    draftArr = draftArr ? draftArr : [];
    tierListArray.template = 'dragAndDrop';
    draftArr.push({draftName: inputText, data: tierListArray});
    try {
        await AsyncStorage.setItem('Drafts', JSON.stringify(draftArr));
        //await this.props.saveDraftAction('');
    } catch (error) {
        Alert.alert(`Error: ${error}`);
    }
    self.setState({settingsModal: false, showDraftModal: false});
};

const rerenderView = self => {
    if(self.renderCount > 0){
        return;
    }
    self.renderCount++;

    const {isDraft, draftImages} = self.props;

    if (isDraft && draftImages) {
        if(!draftImages.list){
            Alert.alert('Sorry, this draft format is no longer supported :(');
            return;
        }
        self.props.tierListAction(draftImages);
    } else {
        const newList = CreateTierList(false, self);
        self.props.tierListAction(newList);
    }
};