import {AdMobInterstitial} from 'expo-ads-admob';

export const advertVideo = async (Config) => {
    if(!Config.isPremium){
        // Display an interstitial
        await AdMobInterstitial.setAdUnitID(Config.android_video_admob_key); // Test ID, Replace with your-admob-unit-id
        await AdMobInterstitial.setTestDeviceID('EMULATOR');
        await AdMobInterstitial.requestAdAsync({ servePersonalizedAds: true});
        await AdMobInterstitial.showAdAsync();
    }
};