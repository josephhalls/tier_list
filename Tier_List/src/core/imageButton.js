import React from 'react';
import {Image, TouchableOpacity} from 'react-native';

export const imageButton = (imageUrl, onPress, additionalParams = {}) => {
    const style = Object.assign({}, {flex: 1}, additionalParams.styling);
    return(
        <TouchableOpacity
            style={{flex: 1, alignContent: 'center', alignItems: 'center'}}
            onPress={() => onPress()}
        >
            <Image
                resizeMode='contain'
                style={style}
                source={imageUrl}
            />
        </TouchableOpacity>
    );
};