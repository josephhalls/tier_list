import {Dimensions} from 'react-native';

export const ComponentWillMountCreate = (self) => {

    self.renderCount = 0;

    const dWidth =  Dimensions.get('window').width,
        dHeight = Dimensions.get('window').height,
        imageWidth = dWidth / 4,
        imageHeight = dHeight / 8;
    self.props.modelAction({imageWidth, imageHeight});
};