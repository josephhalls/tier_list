import {View} from 'react-native';
import React from 'react';
import {ImageCreator} from './ImageCreator';
import {imageButton} from './imageButton';

export const ImageScrollView = (self) => {

    const {selectionPage} = self.state;

    const leftPress = () => {
        if(selectionPage > 0){
            self.setState({selectionPage: (selectionPage - 1)});
        }
    };

    const rightPress = () => {
        const {images, imagesOnSelection} = self.state;
        const endPage = (Math.ceil((images.length / imagesOnSelection)) - 1);
        if(selectionPage < endPage){
            self.setState({selectionPage: (selectionPage + 1)});
        }
    };

    if(!self.state.savingPhotos) {
        return (
            <View style={{flex: 1, flexDirection: 'row'}}>
                <View
                    style={{flex: 1, backgroundColor: 'black'}}
                    onLayout={e => {
                        self.model.leftArrowWidth = e.nativeEvent.layout.width;
                    }}>
                    <View style={{flex: 1}}>
                        <View style={{flex: 0.5}}/>
                        {imageButton(require('../../assets/left_arrow.png'), leftPress)}
                        <View style={{flex: 0.5}}/>
                    </View>
                </View>
                <View
                    style={{flex: 4, flexDirection: 'row'}}
                    onLayout={e => {
                        self.model.imageSelectionHeight = e.nativeEvent.layout.height;
                        self.model.imageSelectionWidth = e.nativeEvent.layout.width;
                    }}>
                    {images(self)}
                </View>
                <View style={{flex: 1, backgroundColor: 'black'}}>
                    <View style={{flex: 1}}>
                        <View style={{flex: 0.5}}/>
                        {imageButton(require('../../assets/right_arrow.png'), rightPress)}
                        <View style={{flex: 0.5}}/>
                    </View>
                </View>
            </View>
        );
    }
};

const images = (self) => {
    const {images, imagesOnSelection} = self.state;
    if(images.length > 0){
        const pageImages = images.filter((image, index) => {
            const imagePosition = self.state.selectionPage * imagesOnSelection;
            if ((index >= imagePosition) && (index <= (imagePosition + (imagesOnSelection - 1)))) {
                return image;
            }
        });
        return pageImages.map((image) => {
            return ImageCreator(image, self, true);
        });
    }
};