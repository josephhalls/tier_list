import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import {EditRow, ImageCreator, modalDialog} from './index';
import {SubmitInput} from './SubmitInput';

export const RowCreator = (self, rowInfo, index, noPanResponder = false) => {
    const {draggingIdx, editingRowId, DeviceOrientation} = self.state;
    const imagesPerRow = DeviceOrientation === 'PORTRAIT' ? 4 : 8;
    const {changeNameToggle, tierListArray} = self.props;
    const section = rowInfo.section;
    const rowPositionIndex = self.state.savingPhotos ? (imagesPerRow + 1) : imagesPerRow;
    const rowFlexHeight = Math.floor(rowInfo.images.length / rowPositionIndex) + 1;
    const rowCreatorHeight = noPanResponder ? {height: self.model.rowHeight} :  {flex : rowFlexHeight};

    const newImageArr = createNewImageArr(rowInfo, self, imagesPerRow);

    const sectionColour = changeNameToggle ? 'white' : section.colour;
    const editingRowIndex = (editingRowId.index) && (editingRowId.index < tierListArray.list.length) ? editingRowId.index : 0;
    const selectedText = changeNameToggle ? tierListArray.list[editingRowIndex].section.text : section.text;

    return(
        <View style={[rowCreatorHeight, {flexDirection: 'row', opacity: draggingIdx === index ? 0 : 1}]}
            onLayout={ e =>{
                self.model.rowHeight = e.nativeEvent.layout.height;
                self.model.rowWidth = e.nativeEvent.layout.width;
            }}
            {...(changeNameToggle ? {} : noPanResponder ? {} : self._panResponder.panHandlers)}>
            <TouchableOpacity
                onLayout={ e =>{self.model.sectionWidth = e.nativeEvent.layout.width;}}
                style={{flex: 1, backgroundColor: sectionColour, justifyContent: 'center'}}
                onPress={() => {
                    if(changeNameToggle){
                        EditRow(rowInfo.id, self);
                    }
                }}>
                <Text style={{color: [section.textColour], fontWeight: 'bold', fontSize: 20, textAlign: 'center', textAlignVertical: 'center',}}>{section.text}</Text>
            </TouchableOpacity>
            <View style={{flex: 5}}>
                {newImageArr.map(images => {
                    return dropImageSection(images, self);
                })}
            </View>
            {self.state.showSectionDialogModal ? modalDialog('Rename your Tier Row', 'Please enter a name', selectedText, self, SubmitInput, self.closeChangeName): <View/>}
        </View>
    );
};

const dropImageSection = (images, self) => {
    return(
        <View style={{flex: 4, flexDirection: 'row', backgroundColor: '#292929', borderWidth: 1, borderColor: 'black'}}
            onLayout={ e =>{
                const {height, width} = e.nativeEvent.layout,
                    {imageDropSectionHeight, imageDropSectionWidth} = self.model,
                    iDSHeight = (height > (imageDropSectionHeight + 1)) || (height < (imageDropSectionHeight - 1)),
                    iDSWidth = (width > (imageDropSectionWidth + 1)) || (width < (imageDropSectionWidth - 1));
                if(iDSHeight || iDSWidth || !imageDropSectionHeight || !imageDropSectionWidth){
                    self.model.imageDropSectionHeight = height;
                    self.model.imageDropSectionWidth = width;
                    self.forceUpdate();
                }
            }}
        >
            {images.map(image => {
                return ImageCreator(image, self);
            })}
        </View>
    );
};

const createNewImageArr = (rowInfo, self, imagesPerRow) => {
    const {savingPhotos} = self.state;
    let tempArr = [];
    let newImageArr = [];
    const rowPositionIndex = savingPhotos && rowInfo.images.length <= imagesPerRow ? 0 : 1;
    rowInfo.images.map((image, index) => {
        tempArr.push(image);
        if(((index + rowPositionIndex) % imagesPerRow === 0) && index !== 0){
            newImageArr.push(tempArr);
            tempArr = [];
        }
    });
    newImageArr.push(tempArr);
    return newImageArr;
};