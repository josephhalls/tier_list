import * as Permissions from 'expo-permissions';
import {advertVideo} from './advertVideo';
import Config from '../../Config';

export const SaveScreenPermission = async (self) => {
    const permission = await Permissions.getAsync(Permissions.CAMERA_ROLL);
    if (permission.status !== 'granted') {
        const newPermission = await Permissions.askAsync(Permissions.CAMERA_ROLL);
        if (newPermission.status === 'granted') {
            self.saveScreen(self);
        }
    } else {
        saveScreen(self);
    }
};

const saveScreen = (self) => {
    advertVideo(Config);
    self.setState({savingPhotos: true});
};