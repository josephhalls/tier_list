import React from 'react';
import {View} from 'react-native';
import TierSection from '../components/common/TierSection';

export const sectionHeaderClass = (self) => {
    return(
        <View style={{flex: 7}}>
            {self.props.tierListArray.list.map((prop, key) => {
                const {draftImages} = self.props;
                const draftImageRow = draftImages ?  draftImages[key] : null;
                return(
                    <TierSection draftImages={draftImageRow} key={key + '34'} tierId={key} />
                );
            })}
        </View>
    );
};