import {Image, TouchableOpacity} from 'react-native';
import {ImagePressed} from './ImagePressed';
import React from 'react';

export const ImageCreator = (image, self, isImageScroll = false) => {
    if(image){

        const imageDimensions = getImageDimensions(self, isImageScroll);

        return(
            <TouchableOpacity
                style={imageDimensions}
                onPress={()=> ImagePressed(image, self)}
            >
                <Image
                    resizeMode={image.style}
                    onLayout={ e =>{
                        self.model.imageHeight = e.nativeEvent.layout.height;
                        self.model.imageWidth = e.nativeEvent.layout.width;
                    }}
                    {...(self.props.changeNameToggle ? {} : self._panResponder.panHandlers)}
                    style={imageDimensions}
                    source={{ uri: image.uri}}
                />
            </TouchableOpacity>
        );
    }
};

const getImageDimensions = (self, isImageScroll) => {
    const {imageDropSectionHeight, imageDropSectionWidth, imageSelectionHeight, imageSelectionWidth} = self.model,
        {imagesOnSelection, DeviceOrientation} = self.state,
        imagesPerRow = DeviceOrientation === 'PORTRAIT' ? 4 : 8;

    if(isImageScroll && imageSelectionHeight && imageSelectionWidth){
        return {height: imageSelectionHeight, width: (imageSelectionWidth / imagesOnSelection)};
    }

    if(imageDropSectionHeight && imageDropSectionWidth){
        return {height: imageDropSectionHeight, width: (imageDropSectionWidth / imagesPerRow)};
    }

    const {imageWidth, imageHeight} = self.props.model;
    return {height: imageHeight, width: imageWidth};
};