import {Animated, PanResponder} from 'react-native';

let currentIndex = 0;

export const DragDrop = (self) => {
    createPanResponder(self);
};

const createPanResponder = (self) => {
    self._panResponder = PanResponder.create({
        // Ask to be the responder:
        onStartShouldSetPanResponder: () => true,
        onStartShouldSetPanResponderCapture: () => true,
        onMoveShouldSetPanResponder: () => true,
        onMoveShouldSetPanResponderCapture: () => true,

        onPanResponderGrant: (evt, gestureState) => {
            const {tierListArray} = self.props;
            //Touching image scroll section
            self.point.setValue({ x: gestureState.x0, y: gestureState.y0 });
            if(gestureState.y0 > self.model.mainDropAreaHeight){
                currentIndex = xToIndex(gestureState.x0, self);
                const selectedImage = self.getImage(currentIndex);
                self.model.selectedImage = selectedImage;
                const {images} = self.state;
                images[currentIndex] = {};
                self.setState({showDraggableImage: true, dragging: true, images, usableImage: selectedImage});
            }
            //Touching image drop section (tiers)
            else if(gestureState.y0 < self.model.mainDropAreaHeight && gestureState.x0 > self.model.sectionWidth){
                const {selectedImage, index, imagePositionIndex} = findSelectedDropImage(gestureState, self);
                if(selectedImage){
                    self.model.selectedImage = selectedImage;
                    const newRowData = removeElementFromArray(tierListArray, index, imagePositionIndex);
                    self.props.tierListAction(newRowData);
                    self.setState({showDraggableImage: true, inATier: true, dragging: true, usableImage: selectedImage});
                }
            }
        },
        onPanResponderMove: (evt, gestureState) => {
            Animated.event([{x: self.point.x, y: self.point.y}])({x: gestureState.moveX, y: gestureState.moveY});
        },
        onPanResponderTerminationRequest: () => false,
        onPanResponderRelease: (evt, gestureState) => {
            dropImage(gestureState, self);
            reset(self);
        },
        onPanResponderTerminate: (evt, gestureState) => {
            dropImage(gestureState, self);
            reset(self);
        },
        onShouldBlockNativeResponder: () => true
    });
};

const dropImage = (gestureState, self) => {
    const {sectionWidth, mainDropAreaHeight, imageWidth} = self.model;
    const currentImageWidth = imageWidth ? imageWidth : self.props.model.imageWidth;
    const xPosition = gestureState.moveX;
    const yPosition = gestureState.moveY;
    let rowData = self.props.tierListArray;
    const level = yToIndex(gestureState.moveY, self);
    const imagePositionIndexCoordinate = Math.floor((gestureState.moveX - sectionWidth) / currentImageWidth);
    const {index, imagePositionIndex} = findWhichImageObject(level, imagePositionIndexCoordinate, self);
    if(self.state.showDraggableImage && xPosition > sectionWidth && yPosition < mainDropAreaHeight && rowData.list.length > index){
        const image = self.state.inATier ? self.model.selectedImage : self.state.usableImage;
        const imageArray = rowData.list[index].images[imagePositionIndex];
        if(!imageArray || (imageArray && imageArray.uri === '')){
            rowData.list[index].images[imagePositionIndex] = image;
        }
        else{
            rowData.list[index].images.splice(imagePositionIndex, 0, image);
        }
        rowData = removeUndefinedElements(rowData);
        self.props.tierListAction(rowData);
    }
    else{
        if(self.model.selectedImage && Object.keys(self.model.selectedImage).length > 0){
            const {images} = self.state;
            images.push(self.model.selectedImage);
            self.setState({images});
        }
    }
};

const findSelectedDropImage = (gestureState, self) => {
    const {imageDropSectionHeight, topOffset, sectionWidth, imageWidth} = self.model;
    const currentImageWidth = imageWidth ? imageWidth : self.props.model.imageWidth;
    const level = Math.floor((gestureState.y0 - topOffset) / imageDropSectionHeight);
    const imagePositionIndex = Math.floor((gestureState.x0 - sectionWidth) / currentImageWidth);

    return findWhichImageObject(level, imagePositionIndex, self);
};

const removeElementFromArray = (rowData, index, imagePositionIndex) => {
    let newRowData = rowData;
    newRowData.list[index].images = rowData.list[index].images.filter((image, i) => {
        if(imagePositionIndex !== i){
            return image;
        }
    });
    return newRowData;
};

const removeUndefinedElements = (rowData) => {
    const newData = rowData.list.filter(row => {
        let newRow = row;
        newRow.images = row.images.filter(function (el) {
            return el != null;
        });
        return newRow;
    });
    return {list: newData};
};

const reset = (self) => {
    const {images} = self.state;
    const newImages = images.filter(image => {
        if(JSON.stringify(image) !== '{}'){
            return image;
        }
    });
    //active = false;
    self.model.selectedImage = {};
    self.setState({dragging: false, draggingIdx: -1, showDraggableImage: false, inATier: false, images: newImages});
};

const findWhichImageObject = (level, imagePositionIndex, self) => {
    const {tierListArray} = self.props;
    const {DeviceOrientation} = self.state;
    const imagesPerRow = DeviceOrientation === 'PORTRAIT' ? 4 : 8;
    let tempIndex = 0;
    let foundData = {};
    tierListArray.list.map((data, index) => {
        let startedIndex = tempIndex;
        let imagesLength = data.images.length;
        let end = false;
        while (end !== true) {
            if(tempIndex === level){
                const differenceIndex = tempIndex - startedIndex;
                const newImagePositionIndex = (differenceIndex * imagesPerRow) + imagePositionIndex;
                foundData = {
                    selectedImage : data.images[newImagePositionIndex],
                    index,
                    imagePositionIndex: newImagePositionIndex
                };
            }
            if(imagesLength >= imagesPerRow){
                imagesLength = imagesLength - imagesPerRow;
                tempIndex ++;
                continue;
            }
            tempIndex ++;
            end = true;
        }
    });
    return foundData;
};

const yToIndex = (gestureState, self) => {
    return Math.floor((gestureState - self.model.topOffset) / self.model.imageDropSectionHeight);
};

const xToIndex = (gestureState, self) => {
    const {imagesOnSelection, selectionPage, DeviceOrientation} = self.state,
        {leftArrowWidth, imageSelectionWidth, listButtonsWidth} = self.model,
        width = imageSelectionWidth / imagesOnSelection;
    const positionStart = selectionPage * imagesOnSelection;
    let value = 0;
    if(DeviceOrientation === 'PORTRAIT'){
        value = positionStart + Math.floor((gestureState - leftArrowWidth) / width);
    }else{
        value = positionStart + Math.floor((gestureState - leftArrowWidth - listButtonsWidth) / width);
    }

    if(value < 0){
        return 0;
    }
    return value;
};