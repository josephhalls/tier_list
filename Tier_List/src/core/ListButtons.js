import {Alert, View} from 'react-native';
import {ListButton} from '../components/common';
import {captureScreenShot} from './captureScreenShot';
import {SaveScreenPermission} from './SaveScreenPermission';
import {getSectionHeaderLetterColour} from './CreateTierList';
import React from 'react';
import {imageButton} from './imageButton';

export const ListButtons = (self) => {
    const {savingPhotos, hideOptionButton, DeviceOrientation} = self.state;
    if (savingPhotos) {
        const backText = hideOptionButton ? '' : 'Back';
        const downloadText = hideOptionButton ? '' : 'Download';
        return(
            <View style={{flexDirection: 'row', flex: 1}}>
                <ListButton btnText={backText} onPress={() => {
                    self.setState({savingPhotos: false});
                }}/>
                <ListButton btnText={downloadText} onPress={() => {
                    self.setState({hideOptionButton: true}, () => {
                        const _this = self;
                        setTimeout(() => {
                            window.requestAnimationFrame(() => {
                                captureScreenShot(() => {
                                    _this.setState({savingPhotos: false, hideOptionButton: false});
                                });
                            });
                        });
                    });
                }}/>
            </View>
        );
    }
    else if(!hideOptionButton){
        const sectionHeaderFlex = DeviceOrientation === 'PORTRAIT' ? 1 : 2;
        const toggleEditImage = self.props.changeNameToggle ? require('../../assets/edit_icon_yellow.png') : require('../../assets/edit_icon.png');
        return (
            <View
                style={{flexDirection: 'row', flex: 1, backgroundColor: '#292929'}}
                onLayout={e => {
                    self.model.listButtonsWidth = e.nativeEvent.layout.width;
                }}>
                <View style={{flex: sectionHeaderFlex}}>
                    <View style={{flex: 0.5}}/>
                    <View style={{flexDirection: 'row', flex: 1}}>
                        {imageButton(require('../../assets/add_row.png'), () => {addSectionHeaders(self); self.forceUpdate();})}
                        {imageButton(require('../../assets/minus_row.png'), () => {removeSectionHeaders(self); self.forceUpdate();})}
                    </View>
                    <View style={{flex: 0.5}}/>
                </View>
                <View style={{flexDirection: 'row', flex: 5}}>
                    <View style={{flex: 0.5}} />
                    <View style={{flexDirection: 'row', flex: 1}}>
                        {imageButton(require('../../assets/image_selector.png'), () => {imageSelector(self);})}
                    </View>
                    <View style={{flex: 1}}>
                        <View style={{flex: 0.5}}/>
                        <View style={{flexDirection: 'row', flex: 1}}>
                            {imageButton(require('../../assets/download_icon.png'), () => {SaveScreenPermission(self);})}
                            {imageButton(toggleEditImage, () => {self.toggleName();})}
                        </View>
                        <View style={{flex: 0.5}}/>
                    </View>
                </View>
                {/*{upgradeOptionDisplay()}*/}
            </View>
        );
    }
};

const addSectionHeaders = (self) => {
    if(self.props.changeNameToggle) {
        Alert.alert('You cannot add tiers in edit mode! Please turn edit mode off.');
        return;
    }

    const {tierListArray} = self.props;
    let array = tierListArray.list;
    const id = array.length;
    if (array.length < 11) {
        const {sectionText, colour, fontColour} = getSectionHeaderLetterColour(id);
        array.push(self.standardObject(id, sectionText, colour, fontColour));
    }
    const listArr = {list: array};
    self.props.tierListAction(listArr);
};

const removeSectionHeaders = (self) => {
    if(self.props.changeNameToggle) {
        Alert.alert('You cannot remove tiers in edit mode! Please turn edit mode off.');
        return;
    }

    let array = self.props.tierListArray.list;
    if (array.length < 2) {
        return;
    }
    array.pop();
    const listArr = {list: array};
    self.props.tierListAction(listArr);
};

const imageSelector = self => {
    let {count} = self.props.multiSelectObj;
    count++;
    self.props.showMultiSelectAction({show: true, count});
};