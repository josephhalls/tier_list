import React from 'react';
import {AdMobBanner} from 'expo-ads-admob';

export const advertBanner = (Config, self) => {
    if(!Config.isPremium && !self.state.savingPhotos){
        return (
            <AdMobBanner
                bannerSize="fullBanner"
                adUnitID={Config.android_list_home_admob_key}
                testDeviceID="EMULATOR"
                servePersonalizedAds // true or false
                onDidFailToReceiveAdWithError={self.bannerError}
            />
        );
    }
};