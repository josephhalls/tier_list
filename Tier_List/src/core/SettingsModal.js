import React from 'react';
import {Modal, View} from 'react-native';
import {OptionMenuButton} from '../components/common/OptionMenuButton';
import Config from '../../Config';
import {upgradeToPremium} from './upgradeToPremium';
import Orientation from 'react-native-orientation';
import {CreateTierList} from './CreateTierList';

export const SettingsModal = (self) => {
    const orientationText = self.state.DeviceOrientation === 'PORTRAIT' ? 'Horizontal View' : 'Portrait View';
    return(
        <Modal
            animationType='slide'
            transparent={true}
            visible={self.state.settingsModal}
            style={{justifyContent: 'center', alignItems: 'center', textAlign: 'center'}}
        >
            <View style={{flex: 1}}/>
            <View style={{flex: 6, flexDirection: 'row'}}>
                <View style={{flex: 1}}/>
                <View style={{flex: 6, backgroundColor: 'black', borderRadius: 20}}>
                    <OptionMenuButton btnText='New Tier List' onPress={() => newTierList(self)}/>
                    <OptionMenuButton btnText='Save Draft' onPress={() => saveDraft(self)}/>
                    {/*<OptionMenuButton btnText='Download Screenshot' onPress={() => {closeMenuModal(self); SaveScreenPermission(self);}}/>*/}
                    <OptionMenuButton btnText={orientationText} onPress={() => {changeScreenOrientation(self); closeMenuModal(self)}}/>
                    <OptionMenuButton btnText='Close' onPress={() => closeMenuModal(self)}/>
                </View>
                <View style={{flex: 1}}/>
            </View>
            <View style={{flex: 1}}/>
        </Modal>
    );
};

const newTierList = (self) => {
    const newList = CreateTierList(false, self);
    self.props.tierListAction(newList);
    closeMenuModal(self);
};

const saveDraft = (self) => {
    if (Config.isPremium) {
        self.setState({settingsModal: false, showDraftModal: true});
    } else {
        upgradeToPremium();
    }
};

const changeScreenOrientation = async (self) => {
    const {DeviceOrientation} = self.state;
    if (!Config.isPremium) {
        upgradeToPremium();
        return;
    }
    if (DeviceOrientation === 'PORTRAIT') {
        self.setState({DeviceOrientation: 'LANDSCAPE'}, () => {
            Orientation.lockToLandscapeLeft();
        });
    } else {
        self.setState({DeviceOrientation: 'PORTRAIT'}, () => {
            Orientation.lockToPortrait();
        });
    }
    self.closeMenuModal();
};

const closeMenuModal = (self) => {
    self.setState({settingsModal: false});
};