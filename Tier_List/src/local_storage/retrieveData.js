import {AsyncStorage} from 'react-native';

export const retrieveData = async () => {
    try {
        const value = await AsyncStorage.getItem('Drafts');
        if (value !== null) {
            const data = JSON.parse(value);
            return data;
        }
    } catch (error) {
        // Error retrieving data
        console.log(`Error: ${error}`);
        return [];
    }
};