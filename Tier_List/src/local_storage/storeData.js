import {AsyncStorage} from 'react-native';
import {retrieveData} from './retrieveData';

export const _storeData = async (draftName, props) => {
    const {tierListArray} = props;
    let draftArr = await retrieveData();
    draftArr = draftArr ? draftArr : [];
    draftArr.push({draftName, data: {list: tierListArray.list}});
    try {
        await AsyncStorage.setItem('Drafts', JSON.stringify(draftArr));
        await props.saveDraftAction('');
    } catch (error) {
        // Error saving data
        console.log(`Error: ${error}`);
    }
};