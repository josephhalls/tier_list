export const TIER_LIST = 'tier_list';
export const SELECTED_PHOTO = 'selected_photo';
export const IS_PHOTO_SELECTED = 'is_photo_selected';
export const SHOW_MODAL = 'show_modal';
export const SELECTED_TIER = 'selected_tier';
export const DELETED_IMAGE = 'deleted_image';
export const SAVE_DRAFT = 'save_draft';
export const IMAGE_STYLE = 'image_style';
export const AUTO_PADDING = 'auto_padding';
export const CHANGE_NAME_TOGGLE = 'change_name_toggle';
export const SHOW_COLOUR_PICKER = 'show_colour_picker';
export const MULTI_SELECT_OBJ = 'multi_select_obj';
export const MODEL = 'model';

//Drag and drop variables
export const SECTION_WIDTH = 'sectionWidth';
export const TOP_OFFSET = 'topOffset';
export const ROW_HEIGHT = 'rowHeight';
export const IMAGE_HEIGHT = 'imageHeight';
export const IMAGE_WIDTH = 'imageWidth';
export const PLUS_WIDTH = 'plusWidth';
export const LEFT_ARROW_WIDTH = 'leftArrowWidth';
export const SCROLL_OFFSET = 'scrollOffset';
export const SELECTED_IMAGE = 'selectedImage';
export const MAIN_DROP_AREA_HEIGHT = 'mainDropAreaHeight';
export const IMAGE_DROP_SECTION_HEIGHT = 'imageDropSectionHeight';
export const IMAGE_DROP_SECTION_WIDTH = 'imageDropSectionWidth';