import React, {Component} from 'react';
import {Animated, BackHandler} from 'react-native';
import {ComponentWillMountCreate, ComponentDidUpdateCreate, RenderView, DragDrop} from '../core';
import {connect} from 'react-redux';
import * as actions from '../actions';
import {Actions} from "react-native-router-flux";
import Orientation from 'react-native-orientation';

class Create extends Component {

    model = {scrollOffset: 0};

    point = new Animated.ValueXY();

    standardObject = (id, text, colour, textColour) => {
        return {id, section: {text: text, colour: colour, textColour}, images: []}
    };

    constructor(props) {
        super(props);
        this.state = {
            draggingIdx: -1,
            images: [],
            showDraggableImage: false,
            inATier: false,
            usableImage: {},
            showSectionDialogModal: false,
            editingRowId: {},
            settingsModal: false,
            savingPhotos: false,
            DeviceOrientation: 'PORTRAIT',
            showDraftModal: false,
            hideOptionButton: false,
            selectionPage: 0,
            imagesOnSelection: 4
        };
        this.handleBackButtonClick = this.backButton.bind(this);
        DragDrop(this);
    }

    componentWillMount() {
        ComponentWillMountCreate(this);
        BackHandler.addEventListener('hardwareBackPress', this.backButton);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.backButton);
    }

    componentDidUpdate(prevProps, prevStates, snapshot) {
        ComponentDidUpdateCreate(this, prevProps);
    }

    render() {
        return RenderView(this);
    }

    getImage = index => {
        return this.state.images[index];
    };

    toggleName = () => {
        this.props.changeNameToggleAction(!this.props.changeNameToggle);
    };

    menuPressed = () => {
        this.setState({settingsModal: true});
    };

    closeChangeName = () => {
        this.setState({showSectionDialogModal: false});
    };

    closeDraftModal = () => {
        this.setState({showDraftModal: false});
    };

    closeDraftDialog = () => {
        this.setState({showDraftModal: false});
    };

    backButton = () => {
        Orientation.lockToPortrait();
        Actions.pop({refresh: {}});
        return true;
    };
}

const mapStateToProps = (state) => {
    const {model} = state.modelReducer;
    return {
        selectedTier: state.selectedTierReducer.selectedTier,
        selectedPhoto: state.selectedPhotoReducer.selectedPhoto,
        tierListArray: state.tierListReducer.tierListArray,
        changeNameToggle: state.changeNameToggleReducer.changeNameToggle,
        selectedColour: state.showModalReducer.showColourPicker,
        multiSelectObj: state.showModalReducer.multiSelectObj,
        dragDropObj: state.dragDropReducer,
        model
    }
};

export default connect(mapStateToProps, actions)(Create);