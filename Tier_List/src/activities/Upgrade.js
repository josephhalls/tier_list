import React, { Component } from 'react';
import { StyleSheet, Text, View, Alert, AsyncStorage, Image, Dimensions, BackHandler } from 'react-native';
import ViewPager from '@react-native-community/viewpager';
import { Button2, Header } from "../components/common";
import { RFValue } from "react-native-responsive-fontsize";
import { AdMobBanner } from 'expo-ads-admob';
import Config from '../../Config';
import * as InAppPurchases from 'expo-in-app-purchases';
import { Actions } from "react-native-router-flux";
import { Platform } from '@unimodules/core';
import {mainHeader} from "../core";

const viewWidth = (Dimensions.get('window').width);

class Upgrade extends Component {

    constructor(props) {
        super(props);
        this.state = {
            pageNumber: 0,
            progress: 0,
            totalAmountOfPages: 4
        };

        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    };

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    render() {
        return (

            <View style={styles.viewStyle}>
                {mainHeader(() => {Actions.pop({refresh: {}});})}
                <View style={styles.viewStyle2}>
                    {this.individualImages()}
                </View>
                {this.advertBanner()}
            </View>
        );
    }

    individualImages = () => {

        const upgrades = [require('../../assets/Upgrade_One_New.png'), require('../../assets/Upgrade_One_Updated.png'), require('../../assets/Upgrade_Two_Updated.png'), require('../../assets/Upgrade_Three_Updated.png'), , require('../../assets/Upgrade_Four_Updated.png')];

        return (
            <ViewPager
                style={{ flex: 9 }}
                showPageIndicator={true}
                onPageSelected={(d) => this.pageChanged(d)}
                initialPage={0}>
                {upgrades.map((source, key) => {
                    return (
                        <View style={{ flex: 1 }} key={(key + 1)}>
                            <View style={{ flex: 0.1 }} ></View>
                            <Image style={styles.imageStyle} resizeMode="contain" source={source}></Image>
                            <View style={{ flex: 0.1 }} ></View>
                        </View>
                    );
                })}
                <View style={{ flex: 1 }} key="4">
                    <View style={styles.textContainer}>
                        <View style={styles.contextview}>
                            <View style={styles.textContainer}>
                                <View style={{ flex: 0.1 }} />
                                <Text style={styles.textBoldWhite}>
                                    Upgrade Unlocks:{'\n'}
                                    {'\n'}
                                    Draft Saving {'\n'}
                                    {'\n'}
                                    Pro Desktop View {'\n'}
                                    {'\n'}
                                    All Advertising Removed {'\n'}
                                    {'\n'}
                                    Complete Upgrade{'\n'}
                                    {'\n'}
                                    One Purchase | Only £2.39{'\n'}
                                    {'\n'}
                                    Full Lifetime Access!
                                </Text>
                                <Button2 btnText="Upgrade / Restore" onPress={() => { this.upgradePressed() }} />
                                <View style={{ flex: 0.1 }} />
                            </View>
                        </View>
                    </View>
                </View>
            </ViewPager>
        )
    }

    pageChanged = (swipeData) => {
        const page = swipeData.nativeEvent.position + 1,
            { totalAmountOfPages } = this.state,
            progress = page === 0 ? 0 : page / totalAmountOfPages;

        this.setState({ pageNumber: page, progress: progress });
    };

    cancelPress = () => {
        Actions.pop();
    };

    handleBackButtonClick() {
        Actions.pop();
        return true;
    }

    advertBanner = () => {
        if (!Config.isPremium) {
            return (
                <AdMobBanner
                    bannerSize="fullBanner"
                    adUnitID={Config.android_upgrade_admob_key}
                    testDeviceID="EMULATOR"
                    servePersonalizedAds // true or false
                    onDidFailToReceiveAdWithError={this.bannerError}
                />
            )
        }
    }

    upgradePressed = async () => {
        try {
            const connectionInfo = await this.checkConnection();

            if (connectionInfo.responseCode === InAppPurchases.IAPResponseCode.OK) {
                const hasPurchased = this.checkPreviousPurchase(connectionInfo);
                if(!hasPurchased){
                    await this.purchaseUpgrade(connectionInfo);
                }
                return;
            }
            else{
                return Alert.alert(`Error: ${InAppPurchases.IAPErrorCode[connectionInfo.errorCode]}`);
            }
        }
        catch (e) {
            Alert.alert(`Error, can't connect to the store!`);
        }
    };

    checkConnection = async () => {
        return await InAppPurchases.connectAsync()
            .catch(async e => {
                if (e.message === 'Already connected to App Store') {
                    return await InAppPurchases.getPurchaseHistoryAsync()
                    .catch(e => {
                        Alert.alert(`Error: ${e}`);
                    });
                }
                Alert.alert(`Error: ${e}`);
            });
    }

    purchaseUpgrade = async () => {
        const productRef = Platform.OS ==='ios' ? 'Premium' : 'premium';
        const product = await InAppPurchases.getProductsAsync([productRef])
            .catch(e => {
                Alert.alert(`Error, ${e}`);
            })

        await InAppPurchases.setPurchaseListener(({ responseCode, results, errorCode }) => {

            // Purchase was successful
            if (responseCode === InAppPurchases.IAPResponseCode.OK) {
                results.forEach(async purchase => {
                    if (!purchase.acknowledged) {
                        this.purchaseProduct(purchase);
                    }
                });
            }
            else{
                Alert.alert(`Error: ${InAppPurchases.IAPResponseCode[responseCode]}`);
                InAppPurchases.disconnectAsync();
            }
        })
        .catch(e => {
            Alert.alert(`Error, ${e}`);
        })

        InAppPurchases.purchaseItemAsync(product.results[0].productId);
    }

    checkPreviousPurchase = history => {
        const hasPurchased = false;
        history.results.forEach(result => {
            if (result.acknowledged) {
                this.messageBox('Already purchased!', 'Restart the app and your purchase will be restored. If not, please send us a message via the details provided on the contact page.');
                this.upgradeApp();
                hasPurchased = true;
            }
        });
        return hasPurchased;
    }

    purchaseProduct = async purchase => {
        console.log(`Successfully purchased ${purchase.productId}`);
        this.upgradeApp();
        // Process transaction here and unlock content...
        this.messageBox('Premium unlocked', 'Premium feature has been unlocked!');
        // Then when you're done
        await InAppPurchases.finishTransactionAsync(purchase, false);
        InAppPurchases.disconnectAsync();
    }

    upgradeApp = async () => {
        await AsyncStorage.setItem('Upgraded', 'true');
        Config.isPremium = true;
        Actions.reset('Home');
    }

    messageBox = async (title, description) => {
        Alert.alert(
            title,
            description,
            [
                { text: 'Back' }
            ],
            { cancelable: false },
        );
    };
}

const styles = StyleSheet.create({
    viewStyle: {
        backgroundColor: 'black',
        flex: 1,
    },
    imageStyle: {
        flex: 1,
        width: viewWidth
    },
    viewStyle2: {
        flex: 10,
    },
    textContainer: {
        flex: 1
    },
    titleText: {
        textAlignVertical: 'center',
        fontSize: RFValue(18),
        color: '#ffffff',
        textAlign: 'center',
    },
    textWhite: {
        textAlignVertical: 'center',
        fontSize: RFValue(17),
        fontFamily: Config.fontFamily,
        textAlign: 'center',
        color: '#ffffff',
        flex: 1

    },
    textBoldWhite: {
        textAlignVertical: 'center',
        fontSize: RFValue(20),
        fontFamily: Config.fontFamily,
        textAlign: 'center',
        color: '#FFFFFF',
        flex: 1
    },
    contextview: {
        flex: 7
    }
});

export default Upgrade;