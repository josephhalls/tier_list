import React, {Component} from 'react';
import {View, Text, StyleSheet, AsyncStorage, ScrollView, Alert, Modal, Input, RefreshControl, Settings, Switch} from 'react-native';
import { Header, Button2, ListButton, Button3, } from '../components/common';
import { RFValue } from "react-native-responsive-fontsize";
import {Actions} from 'react-native-router-flux';
import DialogInput from 'react-native-dialog-input';
import Config from "../../Config";

class ImageSettings extends Component{

    render(){
        return(
            <View style={styles.viewStyle}>
                <Header headerText=""/>
                <View style={styles.viewStyle1}>
                <Text style={styles.textHeader}>Settings</Text>
                <Text>Manually Set Image Styling</Text>
                <Switch value={isSwitchEnabled} 
                onValueChange={(value) => setSwitch(value)}
                trackColor={{true:'red'}}

                />
                </View>
            </View>
        );
    }
}

hookstate = () => {
    const [isSwitchEnabled, setSwitch] = React.useState(false)
}

const styles = StyleSheet.create({
    viewStyle: {
        backgroundColor: 'black',
        flex: 1
    },
    viewStyle1: {
        flex:1,
        alignContent: 'center',
        justifyContent: 'center',
    },
    viewStyle2: {
        flex:1,
        alignContent: 'center',
        justifyContent: 'center',
    },
    textHeader: {
        textAlignVertical: 'center',
        fontSize: RFValue(25),
        fontFamily: Config.fontFamily,
        color: 'white',
        alignContent: 'center',
        textAlign: 'center',
        flex: 1
    },
    text: {
        textAlignVertical: 'center',
        fontSize: RFValue(20),
        fontFamily: Config.fontFamily,
        color: 'white',
      
        alignContent: 'center',
        textAlign: 'center',
        flex: 1
    }
});

export default ImageSettings;