import React, {Component} from 'react';
import {View, Text, StyleSheet, Image, Dimensions, ProgressBarAndroid} from 'react-native';
import ViewPager from '@react-native-community/viewpager';
import { Header, ListButton } from '../components/common';
import { RFValue } from "react-native-responsive-fontsize";
import {AdMobBanner} from 'expo-ads-admob';
import Config from '../../Config';
import {mainHeader} from "../core";
import {Actions} from "react-native-router-flux";

const viewWidth = (Dimensions.get('window').width);

class PreUpgrade extends Component {

    constructor(props) {
        super(props);
        this.state = {
            pageNumber: 0,
            progress: 0,
            totalAmountOfPages: 3
        };
    };

    render(){
        
        return(
            <View style={styles.viewStyle}>
                {mainHeader(() => {Actions.pop({refresh: {}});})}
                <ViewPager 
                    style={{flex : 10}} 
                    showPageIndicator={true}
                    onPageSelected={(d) => this.pageChanged(d)}
                    initialPage={0}>                
                        <View style={{flex : 1}} key="1">
                            <Image style={styles.imageStyle} resizeMode="cover" source={require('../../assets/Upgrade_One.png')}></Image>
                        </View>
                        <View style={{flex : 1}} key="2">
                            <Image style={styles.imageStyle} resizeMode="cover" source={require('../../assets/Upgrade_Two')}></Image>
                        </View>
                        <View style={{flex : 1}} key="3">
                            <Image style={styles.imageStyle} resizeMode="cover" source={require('../../assets/Upgrade_Three')}></Image>
                        </View>
                        <View style={{flex : 1}} key="4">
                            {/* <HomeMenuButton style={{flex:1}} btnText="Upgrade" onPress={() => {Actions.PreUpgrade()}}/> */}
                        </View>
                        
                </ViewPager>
                <ProgressBarAndroid
                    styleAttr="Horizontal"
                    indeterminate={false}
                    progress={this.state.progress}
                    color={color='white'}
                />
                {this.advertBanner()}
            </View>
        )
    }

    pageChanged = (swipeData) => {
        const page = swipeData.nativeEvent.position + 1,
        {totalAmountOfPages} = this.state,
        progress = page === 0 ? 0 : page / totalAmountOfPages;

        this.setState({pageNumber: page, progress: progress});
    }

    advertBanner = () => {
        if(!Config.isPremium){
            return (
                <AdMobBanner
                        bannerSize="fullBanner"
                        adUnitID={Config.android_instructions_home_admob_key}
                        testDeviceID="EMULATOR"
                        servePersonalizedAds // true or false
                        onDidFailToReceiveAdWithError={this.bannerError} 
                />
            )
        }
    }
}

const styles = StyleSheet.create({

    viewStyle:{
        backgroundColor: 'black',
        flex:1,
    },
    imageStyle: {
        flex:1,
        width: viewWidth
    }
});

export default PreUpgrade;