import React, {Component} from 'react';
import {View, Text, StyleSheet, AsyncStorage, ScrollView, Alert, Modal, Input, RefreshControl} from 'react-native';
import { Header} from '../components/common';
import { RFValue } from "react-native-responsive-fontsize";
import DraftSection from '../components/common/DraftSection';
import {Actions} from 'react-native-router-flux';
import DialogInput from 'react-native-dialog-input';
import Config from "../../Config";
import {mainHeader} from "../core";

class Draft extends Component{

    state = {
        drafts: [],
        showModal: false,
        refreshing: false,
        me: "gg"
    };

    componentWillMount(){
        this._retrieveData();
    }

    scrollToTopAndRefresh() {
        this.flatlistref.scrollToOffset({y: 0, animated: true});
        this.setState({refreshing: true}, this.doRefresh);
    }

    doRefresh = () => {
        this._retrieveData();
        this.setState({me: "true"})
    };

    menuPressed = () => {
        Alert.alert(
            'Edit Tier Row',
            'Exit Edit Mode to apply your colour changes',
            [
                {
                    text: 'Refresh',
                    onPress: this.doRefresh
                },
                {
                    text: 'Cancel',
                }
            ]
        )
    };

    render(){
        return(
            <View style={styles.viewStyle}>
                {mainHeader(() => {Actions.pop({refresh: {}});}, {show: true, menuPressed: this.menuPressed})}
                <View style={styles.viewStyle2}>
                    <ScrollView>
                        {this.draftViews()}
                    </ScrollView>
                </View>
                {this.state.showModal ? this.modal(): <View />}
            </View>
        );
    }

    modal = () => {
        return (
                <DialogInput isDialogVisible={this.state.showModal}
                        title={"Draft name"}
                        message={"Please enter the draft name"}
                        hintInput ={"Pokemon Tier List"}
                        submitInput={ (inputText) => {this.draftRename(inputText);} }
                        closeDialog={ () => {this.setState({showModal: false})}}>
                </DialogInput>
        )
    };

    draftRename = inputText => { 
    };

    draftViews = () => {
        return this.state.drafts.map((draft, i) => {
            const draftName = draft.draftName;
            return(
                <DraftSection 
                    key={i}
                    i={i}
                    draftName={draftName}
                    onPress={() => this.onPress(draft)}
                    closeOnPress={() => this.deleteDraft(i)}
                />
            )
        })
    };

    onPress = (draftsObject) => {
        let draftImages = draftsObject.data;
        if(draftsObject.data.template === 'dragAndDrop'){
            Actions.Create({isDraft: true, draftImages: draftsObject.data});
            return;
        }
        if(Array.isArray(draftsObject.data.list[0])){
            let draftDataArray = this.checkOldCode(draftsObject);
            draftImages = draftDataArray.changedDraftObject.data;
            AsyncStorage.setItem('Drafts', JSON.stringify(draftDataArray.newDraftsObject));
        }
        Actions.List({isDraft: true, draftImages: draftImages});
    };

    checkOldCode = draftsObject => {
        const updatedDraft = this.updateDraftListArr(draftsObject),
            oldDraftsObject = this.state.drafts;
        return this.replaceDraftListArr(oldDraftsObject, updatedDraft, draftsObject.draftName);
    };

    updateDraftListArr = draftsObject => {
        return draftsObject.data.list.map(draft => {
            const firstItem = draft[0],
                draftImages = draft;
            draftImages.shift();
            return {sectionName: firstItem, images: draftImages}
        });
    };

    replaceDraftListArr = (oldDraftsObject, updatedDraft, draftName) => {
        let newDraftsObject = {};
        newDraftsObject.newDraftsObject = oldDraftsObject.map(oldDraftObject => {
            if(oldDraftObject.draftName === draftName){
                oldDraftObject.data.list = updatedDraft;
                newDraftsObject.changedDraftObject = oldDraftObject;
            }
            return oldDraftObject;
        });
        return newDraftsObject;
    };

    deleteDraft = (i) => {
        Alert.alert(
            'Delete this draft?',
            '',
            [
              {
                  text: 'Delete', 
                   onPress: () =>  this.removeFromArray(i)
              },
            //   {
            //       text: 'Rename',
            //       onPress: () =>  { this.setState({showModal: true})}

            //   },
              {text: 'Cancel'}
            ],
            {cancelable: false},
          );
    };

    removeFromArray = (i) => {
        let arr = this.state.drafts;
        arr.splice(i, 1);
        this._storeData(arr);
        this.setState({drafts: arr});
    };

    _retrieveData = async () => {
        try {
          const value = await AsyncStorage.getItem('Drafts');
          if (value !== null) {
            // We have data!!
            const data = JSON.parse(value);
            this.setState({drafts: data});
          }
        } catch (error) {
          // Error retrieving data
          Console.log("Error: ", error);
          return [];
        }
      };

      _storeData = async (draftArr) => {
        try {
          await AsyncStorage.setItem('Drafts', JSON.stringify(draftArr));
        } catch (error) {
          // Error saving data
          Console.log("Error!");
        }
      };

}

const styles = StyleSheet.create({
    viewStyle: {
        backgroundColor: 'black',
        flex: 1
    },
    viewStyle2: {
        flex: 10,
        alignContent: 'center',
        justifyContent: 'center'
    },
    text: {
        textAlignVertical: 'center',
        fontSize: RFValue(20),
        fontFamily: Config.fontFamily,
        color: 'white',
        textAlign: 'center',
        flex: 1
    }
});

export default Draft;