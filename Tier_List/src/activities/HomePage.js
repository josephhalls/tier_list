import React, {Component} from 'react';
import {StyleSheet, Text, View, Alert, Dimensions} from 'react-native';
import {HomeMenuButton} from "../components/common";
import {Actions} from 'react-native-router-flux';
import { RFValue } from "react-native-responsive-fontsize";
import {AdMobBanner} from 'expo-ads-admob';
import Orientation from 'react-native-orientation';
import Config from '../../Config';
import CheckPremium from '../../CheckPremium';
import CreateInstructions from "./CreateInstructions";

const viewHeight = (Dimensions.get('window').height);
  
class HomeScreen extends Component {

    componentWillMount(){
        Orientation.lockToPortrait();
    }

    render(){
        return(
            <View style={styles.container}>
                <View style={styles.viewTopMargin}/>
                    <View style={styles.viewStyle}>
                        {this.CheckIsPremium()}
                    </View>
                <View style={styles.viewStyle2}>
                    <HomeMenuButton btnText="Create" onPress={() => {Actions.Create()}}/>
                    <HomeMenuButton btnText="Instructions" onPress={() => {Actions.CreateInstructions()}}/>
                    {this.draftDisplay()}
                    {this.upgradeDisplay()}
                    <HomeMenuButton btnText="Contact" onPress={() => {Actions.Contact()}}/>
                    {/* <HomeMenuButton btnText="Settings" onPress={() => {Actions.ImageSettings()}}/> */}
                </View>
                {this.advertBanner()}
            </View>
        )
    }
    

    advertBanner = () => {
        if(!Config.isPremium){
            return (
                <AdMobBanner
                        bannerSize="fullBanner"
                        adUnitID={Config.android_list_home_admob_key}
                        testDeviceID="EMULATOR"
                        servePersonalizedAds // true or false
                        onDidFailToReceiveAdWithError={this.bannerError} 
                />
            )
        }
    };

    draftDisplay = () => {
        return(
            <HomeMenuButton btnText="Drafts" onPress={() => {Config.isPremium ? Actions.Draft() : this.upgradeToPremium()}}/>
        )
    };

    upgradeDisplay = () => {
        if(!Config.isPremium){
            return(
                <HomeMenuButton btnText="Upgrade" onPress={() => {Actions.Upgrade()}}/>
            )
        }
    };

    upgradeToPremium = () => {
        Alert.alert(
            'Upgrade Unlock!',
            'Upgrade to Access this feature!',
            [
              {text: 'Back'}
            ],
            {cancelable: false},
          );
    };

    CheckIsPremium = () => {
        if(!Config.isPremium)
        {
            return(
            <Text style={styles.titleText}>Tier List Create</Text>
            )
        }
        return(
        <Text style={styles.titleText}>Tier List Pro</Text>
        )
    }
      
}

const styles = StyleSheet.create({
    viewTopMargin: {
        flex: 0.5
    },
    container: {
        flex: 1,
        backgroundColor: 'black',
        justifyContent: 'center',
    },
    viewStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: "center"
    },
    viewStyle2: {
        flex: 4,
    },
    lockStyle: {
        height: viewHeight / 12,
        paddingTop: 90,
        flex: 1
    },
    lockView: {
        justifyContent: 'center',
        height: viewHeight / 10,
        flexDirection: 'row'
    },
    titleText: {
        fontSize: RFValue(35),
        fontFamily: Config.fontFamily,
        color: 'white',
        textAlign: 'center',
    },
});

export default HomeScreen;