import * as InAppPurchases from 'expo-in-app-purchases';
import Config from '../../Config';
import {AsyncStorage} from 'react-native'

export default Loading = async (self, callback) => {

    const isUpgraded = await AsyncStorage.getItem('Upgraded') === 'true';

    if(isUpgraded){
        Config.isPremium = true;
        return callback();
    }

    callback();
    await checkInappPurchase(self);

}

const checkInappPurchase = async (self) => {
    await InAppPurchases.connectAsync().then( history => {
        if (history.responseCode === InAppPurchases.IAPResponseCode.OK) {
            history.results.forEach(result => {
                if(result.acknowledged){
                    Config.isPremium = true;
                    AsyncStorage.setItem('Upgraded', 'true');
                    self.forceUpdate();
                }
            });
        }
    }).then(() => {
        InAppPurchases.disconnectAsync();
    })
};