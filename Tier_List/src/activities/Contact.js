import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Header} from "../components/common";
import { RFValue } from "react-native-responsive-fontsize";
import {AdMobBanner} from 'expo-ads-admob';
import Config from '../../Config';
import {mainHeader} from "../core";
import {Actions} from "react-native-router-flux";

class Contact extends Component {

    constructor(props) {
        super(props);
    };

    render(){
        return(
            <View style={styles.viewStyle}>
                {mainHeader(() => {Actions.pop({refresh: {}});})}
                <View style={styles.viewStyle2}>
               
                <Text style={styles.text}>
                       Hey there! {'\n'}
                    </Text>
                    <Text style={styles.text}>
                        A few quirks may still appear.{'\n'}
                        If you experience any problems{'\n'}
                        with this app, please don't {'\n'}
                        hesitate to get in touch {'\n'}
                        
                    </Text>
                    <Text style={styles.text}>contacting.futura@gmail.com {'\n'} </Text>
                    <Text style={styles.text}>- Futura Team, UK</Text>

                </View>
                {this.advertBanner()}
            </View>
        );
    }

    advertBanner = () => {
        if(!Config.isPremium){
            return (
                <AdMobBanner
                        bannerSize="fullBanner"
                        adUnitID={Config.android_contact_admob_key}
                        testDeviceID="EMULATOR"
                        servePersonalizedAds // true or false
                        onDidFailToReceiveAdWithError={this.bannerError} 
                />
            )
        }
    }
}

const styles = StyleSheet.create({
    viewStyle: {
        backgroundColor: 'black',
        flex: 1
    },
    viewStyle2: {
        flex: 10,
        alignContent: 'center',
        justifyContent: 'center'
    },
    text: {
        textAlignVertical: 'center',
        fontSize: RFValue(19),
        fontFamily: Config.fontFamily,
        color: 'white',
        textAlign: 'center',
    }
});

export default Contact;