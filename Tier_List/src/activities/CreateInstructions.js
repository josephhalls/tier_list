import React, {Component} from 'react';
import {View, Text, StyleSheet, Image, Dimensions, ProgressBarAndroid} from 'react-native';
import ViewPager from '@react-native-community/viewpager';
import { Header, ListButton } from '../components/common';
import { RFValue } from "react-native-responsive-fontsize";
import {AdMobBanner} from 'expo-ads-admob';
import Config from '../../Config';
import {mainHeader} from '../core/mainHeader';
import {Actions} from "react-native-router-flux";

const viewWidth = (Dimensions.get('window').width);

class CreateInstructions extends Component {

    constructor(props) {
        super(props);
        this.state = {
            pageNumber: 0,
            progress: 0,
            totalAmountOfPages: 6
        };
    };

    render(){
        
        return(
            <View style={styles.viewStyle}>
                {mainHeader(() => {Actions.pop({refresh: {}});})}
                <View style={{flex : 10}} >
                    {this.individualImages()}
                </View>
                <ProgressBarAndroid
                    styleAttr="Horizontal"
                    indeterminate={false}
                    progress={this.state.progress}
                    color={color='white'}
                />
                {this.advertBanner()}
            </View>
        )
    }

    individualImages = () => {

        const instructions = [
            require('../../assets/instruction_images/0_page_instructions.png'),
            require('../../assets/instruction_images/1_page_instructions.png'),
            require('../../assets/instruction_images/2_page_instructions.png'),
            require('../../assets/instruction_images/3_page_instructions.png'),
            require('../../assets/instruction_images/4_page_instructions.png'),
            require('../../assets/instruction_images/5_page_instructions.png')
        ];

        return(
            <ViewPager 
                style={{flex : 9}} 
                showPageIndicator={true}
                onPageSelected={(d) => this.pageChanged(d)}
                initialPage={0}>
                {instructions.map((source, key) => {
                    const style = 0; // key === 1 || key === 10 ? 0 : 0.3;
                    const style2 = 0.1 // key === 1 || key === 10 ? 0.1 : 0.3;
                    return (
                        <View style={{flex : 1}} key={(key + 1)}>
                            <View style={{flex : style}} ></View>
                            <Image style={styles.imageStyle} resizeMode="contain" source={source}></Image>
                            <View style={{flex : style2}} ></View>
                        </View>
                    );
                })}
            </ViewPager>
        )
    };

    pageChanged = (swipeData) => {
        const page = swipeData.nativeEvent.position + 1,
        {totalAmountOfPages} = this.state,
        progress = page === 0 ? 0 : page / totalAmountOfPages;

        this.setState({pageNumber: page, progress: progress});
    };

    advertBanner = () => {
        if(!Config.isPremium){
            return (
                <AdMobBanner
                        bannerSize="fullBanner"
                        adUnitID={Config.android_instructions_home_admob_key}
                        testDeviceID="EMULATOR"
                        servePersonalizedAds // true or false
                        onDidFailToReceiveAdWithError={this.bannerError} 
                />
            )
        }
    }
}

const styles = StyleSheet.create({

    viewStyle:{
        backgroundColor: 'black',
        flex:1,
    },
    imageStyle: {
        flex:1,
        width: viewWidth
    }
});

export default CreateInstructions;