import React from 'react';
import {Scene, Router} from 'react-native-router-flux';
import HomePage from './activities/HomePage';
import Upgrade from './activities/Upgrade';
import Create from './activities/Create';
import Contact from './activities/Contact';
import Draft from './activities/Draft';
import CreateInstructions from './activities/CreateInstructions';
import ImageSettings from './activities/ImageSettings';
import { Settings } from 'react-native';

const RouterComponent = () => {
    return(
        <Router>
            <Scene key="root">
                <Scene key="Home" component={HomePage} hideNavBar initial/>
                {/*<Scene key="List" drawerLockMode='locked-closed' gesturesEnabled={false} component={List} hideNavBar />*/}
                <Scene key="Create" drawerLockMode='locked-closed' gesturesEnabled={false} component={Create} hideNavBar />
                <Scene key="Draft" component={Draft} hideNavBar />
                <Scene key="Upgrade" component={Upgrade} hideNavBar />
                <Scene key="Contact" component={Contact} hideNavBar />
                <Scene key="CreateInstructions" component={CreateInstructions} hideNavBar />
                <Scene key="ImageSettings" component={ImageSettings} hideNavBar />

            </Scene>
        </Router>
    );
};

export default RouterComponent;