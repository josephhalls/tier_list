import React from 'react';
import {View, Text, Image, Dimensions} from 'react-native';

const viewHeight = (Dimensions.get('window').height) / 2;

const SectionDetails = ({details}) => {
    return(
        <View style={styles.viewStyle}>
            <Image style={styles.imageStyle} source={{uri: details.imageUrl}}/>
            <View style={styles.textViewStyle}>
                <Text style={styles.priceStyle}>£{details.price}</Text>
                <Text style={styles.textStyle}>{details.name}</Text>
                <Text style={styles.textStyle}>Based in {details.location}</Text>
            </View>
        </View>
    );
};

const styles = {
    viewStyle: {
        height: viewHeight
        //flex: 1
    },
    imageStyle: {
        flex: 2
    },
    textViewStyle: {
        flex: 1
    },
    priceStyle: {
        fontSize: 30,
        color: '#6b1590',
        paddingTop: 20,
        paddingLeft: 20,
        paddingBottom: 10
    },
    textStyle: {
        paddingLeft: 20
    }
};

export {SectionDetails}