import React, {Component} from 'react';
import { Modal, TouchableOpacity, FlatList, StyleSheet, View, Image, Text} from 'react-native';
import { Header } from "./Header";
import { RFValue } from "react-native-responsive-fontsize";
import {connect} from 'react-redux';
import * as actions from '../../actions';
import * as Permissions from 'expo-permissions';
import CameraRoll, {getAlbums} from "@react-native-community/cameraroll";

class CameraModalMultiSelect extends Component {

    initialState = {
        photos: [],
        albums: [],
        lastCursor: null,
        load: true,
        imageArray: [],
        showAlbum: true,
        album: '',
        maxPicLoad: 30
    };

    constructor(props) {
        super(props);
        this.state = this.initialState;
    };

    openCount = -1;

    async componentDidUpdate() {
        let {multiSelectObj} = this.props;
        if(multiSelectObj.show && this.state.load){
            this.openCount++;
            const permission = await Permissions.getAsync(Permissions.CAMERA_ROLL);
            if (permission.status !== 'granted') {
                const newPermission = await Permissions.askAsync(Permissions.CAMERA_ROLL);
                if (newPermission.status === 'granted') {
                    this._handleButtonPress();
                }
            } else {
                this._handleButtonPress();
            }
        }
    }

    render() {
        const {showAlbum, photos, albums, album} = this.state;
        if(albums.length > 0 || photos.length > 0){
            const data = showAlbum ? albums : photos;
            const renderFunc = showAlbum ? this.albums : this.wrapData;
            return (
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.props.multiSelectObj.show}
                    onDismiss={() => {
                        this.setState({photos: [], lastCursor: null, load: true});
                    }}>
                    <View style={styles.viewStyle}>
                        <Header
                            headerText="Images"
                            rightText='Select'
                            onPress={() => {this.selectPressed()}}
                            cancelOnPress={() => {
                                if(showAlbum){
                                    let {multiSelectObj} = this.props;
                                    multiSelectObj.show = false;
                                    this.props.showMultiSelectAction(multiSelectObj);
                                    this.initialState.imageArray =[];
                                    this.setState(this.initialState);
                                    return;
                                }
                                this.setState({photos: [], lastCursor: null, showAlbum: true});
                            }
                            } />
                        <View style={styles.viewStyle2}>
                            <FlatList
                                extraData={this.state}
                                numColumns={3}
                                data={data}
                                initialNumToRender={18}
                                renderItem={renderFunc}
                                keyExtractor={(item, index) => index.toString()}
                                onEndReachedThreshold={5}
                                onEndReached={({ distanceFromEnd }) => {
                                    if(showAlbum || distanceFromEnd < 0){
                                        return;
                                    }
                                    this.clickAlbum(album, this.state.maxPicLoad);
                                }}
                            />
                        </View>
                    </View>
                </Modal>
            );
        }
        else{
            return(
                <View />
            )
        }

    }

    selectPressed = () => {
        this.props.photoChangedAction({images: this.state.imageArray});
        let {multiSelectObj} = this.props;
        multiSelectObj.show = false;
        this.props.showMultiSelectAction(multiSelectObj);
        this.initialState.imageArray =[];
        this.setState(this.initialState);
    };

    _handleButtonPress = () => {
        this.getAlumList();
    };

    getAlumList = () => {
        CameraRoll.getAlbums({assetType: 'Photos'})
            .then(albums => {
                albums.unshift({title: '', count: this.state.maxPicLoad}); //Adds all album
                Promise.all(albums.map(album => {
                    return new Promise((resolve, reject) => {
                        CameraRoll.getPhotos({first: 1, assetType: 'Photos', groupName: album.title})
                            .then(r => {
                                r.edges.map(image => {
                                    resolve({uri: image.node.image.uri, title: album.title, count: album.count});
                                });
                            });
                    });
                }))
                    .then(data => {
                    this.setState({albums: data, lastCursor: null, load: false, showAlbum: true});
                });
            })
            .catch( err => {
                this.clickAlbum('', this.state.maxPicLoad);
                console.log(err);
            });
    };

    albums = data => {
        const {title, uri, count} = data.item;
        let albumName = title ? title : 'All';
        return(
            <TouchableOpacity
                key={uri}
                style={{flex:1, //here you can use flex:1 also
                    aspectRatio:1,
                    borderWidth: 5}}
                onPress={() => {this.clickAlbum(title, count);}}
            >
                <Image
                    resizeMode="cover"
                    key={uri}
                    style={{flex: 1}}
                    source={{ uri: uri}}
                />
                <Text style={{flex: 1, color: 'white', textAlign: 'center'}} >{albumName}</Text>
            </TouchableOpacity>
        );
    };

    wrapData = ({item}) => {
        let image = item;
        if(image){
            const uri = image.node.image.uri,
                isSelectedImage = this.checkImage(uri),
                borderColour = isSelectedImage ? 'red' : 'transparent';
            return (
                <TouchableOpacity
                    key={uri}
                    style={{flex:1/3, //here you can use flex:1 also
                        aspectRatio:1,
                        borderColor: borderColour,
                        borderWidth: 5}}
                    onPress={() => {this.photoPressed(uri);}}
                >
                    <Image
                        resizeMode="cover"
                        key={uri}
                        style={{flex: 1}}
                        source={{ uri: uri}}
                    />

                </TouchableOpacity>
            );
        }
    };

    clickAlbum = (album, count) => {
        const {lastCursor, maxPicLoad} = this.state;

        let options = {
            first: count > maxPicLoad ? maxPicLoad : count,
            assetType: 'Photos',
            groupName: album
        };

        if(lastCursor){
            options.after = lastCursor;
        }

        CameraRoll.getPhotos(options)
            .then(r => {
                let arr = this.state.photos;
                const newArr = arr.concat(r.edges);
                this.setState({ photos: newArr, lastCursor: r.page_info.end_cursor, load: false, showAlbum: false, album });
            })
            .catch( err => {
                console.log(err);
            });
    };

    photoPressed = (uri) => {
        const count = this.props.selectedPhoto.photoCount++,
            doesImageExist = this.checkImage(uri);

        let {imageArray} = this.state;

        if(doesImageExist){
            imageArray = imageArray.filter(image => {
                if(image.uri !== uri){
                    return uri;
                }
            })
        }else{
            imageArray.push({uri, photoCount: count, style: 'cover'});
        }
        this.setState({imageArray});
    };

    checkImage = uri => {
        let isImageSelected = false;
        this.state.imageArray.map(image => {
            if(image.uri === uri){
                isImageSelected = true;
            }
        });
        return isImageSelected;
    };
}

const styles = StyleSheet.create({
    viewStyle: {
        backgroundColor: 'black',
        flex: 1
    },
    viewStyle2: {
        flex: 10,
        alignContent: 'center',
        justifyContent: 'center'
    },
    text: {
        textAlignVertical: 'center',
        fontSize: RFValue(25),
        color: 'white',
        textAlign: 'center',
    }
});

const mapStateToProps = (state) => {
    return{
        modalVisible: state.showModalReducer.showModal,
        selectedPhoto: state.selectedPhotoReducer.selectedPhoto,
        multiSelectObj: state.showModalReducer.multiSelectObj
    }
};

export default connect(mapStateToProps, actions)(CameraModalMultiSelect);