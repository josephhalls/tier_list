import React from 'react';
import {Text, TouchableOpacity} from 'react-native';
import { RFValue } from 'react-native-responsive-fontsize';
import Config from '../../../Config';

//const viewHeight = (Dimensions.get('window').height);

const ListButton = ({btnText, onPress}) => {
    return(
        <TouchableOpacity activeOpacity={1} style={styles.viewStyle} onPress={onPress}>
            <Text style={styles.textStyle}>{btnText}</Text>
        </TouchableOpacity>
    );
};

const styles = {
    viewStyle: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: 'black'
    },
    textStyle: {
        color: 'white',
        textAlign: 'center',
        fontSize: RFValue(18),
        fontFamily: Config.fontFamily
    }
};

export {ListButton};