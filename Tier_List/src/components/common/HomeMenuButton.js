import React, {Component} from 'react';
import {View, Text, TouchableOpacity, Dimensions, Image} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import Config from '../../../Config';

const viewHeight = (Dimensions.get('window').height);

const HomeMenuButton = ({btnText, onPress, locked}) => {
    if(locked){
        return(
            <View style={styles.lockView}>
                <View style={{flex: 1, alignContent: 'flex-end', alignItems: 'flex-end'}}>
                    <View style={{flex: 1}} />
                    <View style={{flex: 1, flexDirection: 'row'}} >
                        <View style={{flex: 3}} />
                        <Image style={styles.lockStyle} resizeMode='contain' source={require('../../../assets/white_lock.png')} />
                    </View>
                    <View style={{flex: 1}} />
                </View>
                <TouchableOpacity style={styles.viewStyle} onPress={onPress}>
                    <Text style={[styles.textStyle, {textAlign: 'left'}]}>{btnText}</Text>
                </TouchableOpacity>
            </View>
        )
    }
    else{
        return(
            <TouchableOpacity style={styles.viewStyle} onPress={onPress}>
                    <Text style={styles.textStyle}>{btnText}</Text>
            </TouchableOpacity>
        )
    }
};

const styles = {
    viewStyle: {
        borderWidth: 1,
        flex: 1,
        //height: viewHeight / 10,
        borderColor: 'black',
        justifyContent: 'center',
        backgroundColor: 'transparent'
    },
    textStyle: {
        color: 'white',
        textAlign: 'center',
        fontSize: RFValue(25),
        fontFamily: Config.fontFamily
    },
    lockView: {
        flex: 1,
        justifyContent: 'center',
        flexDirection: 'row'
    },
    lockStyle: {
        height: undefined,
        flex: 1,

    },
   
};

export {HomeMenuButton};