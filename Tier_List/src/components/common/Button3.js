import React from 'react';
import {Text, TouchableOpacity} from 'react-native';
import {RFValue } from 'react-native-responsive-fontsize';
import Config from '../../../Config';

//const viewHeight = (Dimensions.get('window').height);

const Button3 = ({btnText, onPress}) => {
    return(
        <TouchableOpacity style={styles.viewStyle} onPress={onPress}>
            <Text style={styles.textStyle}>{btnText}</Text>
        </TouchableOpacity>
    );
};

const styles = {
    viewStyle: {
        height: 70,
        justifyContent: 'center',
        backgroundColor: 'black',
    },
    textStyle: {
        color:'#37e68b',
        textAlign: 'center',
        fontSize: RFValue(23),
        fontFamily: Config.fontFamily
    }
};

export {Button3};