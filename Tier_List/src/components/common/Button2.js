import React from 'react';
import {Text, TouchableOpacity} from 'react-native';
import { RFValue } from 'react-native-responsive-fontsize';
import Config from '../../../Config';

//const viewHeight = (Dimensions.get('window').height);

const Button2 = ({btnText, onPress}) => {
    return(
        <TouchableOpacity style={styles.viewStyle} onPress={onPress}>
            <Text style={styles.textStyle}>{btnText}</Text>
        </TouchableOpacity>
    );
};

const styles = {
    viewStyle: {
        borderWidth: 1,
        borderRadius:8,
        height: 70,
        borderColor: 'black',
        padding: 5,
        paddingLeft: 20,
        paddingRight: 20,
        justifyContent: 'center',
        marginTop: 10,
        marginLeft: 10,
        marginRight: 12,
        backgroundColor: '#37e68b'
    },
    textStyle: {
        color: 'black',
        textAlign: 'center',
        fontSize: RFValue(23),
        fontFamily: Config.fontFamily
    }
};

export {Button2};