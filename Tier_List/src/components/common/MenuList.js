import React, {Component} from 'react';
import {FlatList} from 'react-native';
import {SectionDetails} from './SectionDetails';

class MenuList extends Component{
    render(){
        return(
            <FlatList

                data={[{key: 'a', name: 'Jack', price: '200.00', imageUrl: 'https://3c1703fe8d.site.internapcdn.net/newman/gfx/news/hires/2018/2-dog.jpg', location: 'Ipswich'},
                    {key: 'b', name: 'Charlie', price: '400.00', imageUrl: 'https://i.kinja-img.com/gawker-media/image/upload/s--4vlfc0Vs--/c_scale,f_auto,fl_progressive,q_80,w_800/zhdfbwvbc2miyqyaryl9.jpg', location: 'Colchester'},
                    {key: 'c', name: 'Scooby', price: '500.00', imageUrl: 'https://i.redd.it/j7rtz17ywm8z.jpg', location: 'Stutton'},
                    {key: 'd', name: 'Poppy', price: '100.00', imageUrl: 'https://i.kinja-img.com/gawker-media/image/upload/s--HqfzgkTd--/c_scale,f_auto,fl_progressive,q_80,w_800/wp2qinp6fu0d8guhex9v.jpg', location: 'Stutton'},
                ]}
                renderItem={({item}) => <SectionDetails details={item}/>}
            />
        );
    }
}

export {MenuList};