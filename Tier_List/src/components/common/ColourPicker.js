import React, {Component} from 'react';
import {Modal, StyleSheet, View, Text, TextInput, BackHandler, Dimensions} from 'react-native';
import { Header } from "./Header";
import { RFValue } from "react-native-responsive-fontsize";
import {connect} from 'react-redux';
import * as actions from '../../actions';
import { ColorPicker } from 'react-native-color-picker';

const textInputHeight = (Dimensions.get('window').height) / 12;

class ColourPicker extends Component {
    
    constructor(props) {
      super(props);
      this.state = {
          showEnterCode: false,
          newCode: '#'
      };
    };

    componentDidUpdate(prevProps) {
        const {show, colourCode} = this.props.modalDetails;
        if(show !== prevProps.modalDetails.show){
            this.setState({newCode : colourCode});
        }
    }

    render() {
        return (
            <Modal
            animationType="slide"
            transparent={false}
            visible={this.props.modalDetails.show}
            onDismiss={() => {

            }}>
                <View style={styles.viewStyle}>
                    <Header headerText={`${this.props.modalDetails.type} colour`} cancelOnPress={this.cancelPress} />
                    <View style={styles.viewStyle2}>
                        <Text style={{flex: 1, color: 'white', textAlign: 'center', textAlignVertical: 'center'}}>Either enter a code or use the colour wheel</Text>
                        <TextInput
                            label="Email"
                            style={{ backgroundColor: '#ededed', height: textInputHeight, textAlign: 'center' }}
                            value={this.state.newCode}
                            onChangeText={text => this.setState({newCode: text})}
                        />
                        <View style={{flex: 0.3}}/>
                        <Text
                            style={{flex: 1, color: 'white', textAlignVertical: 'center', textAlign: 'center', backgroundColor: 'grey'}}
                            onPress={() => this.submitColour(this.state.newCode)}
                        >Click here to use code
                        </Text>
                        <Text style={{flex: 1, color: 'white', textAlign: 'center', textAlignVertical: 'center'}}>Press the circle to select from the colour wheel</Text>
                        <ColorPicker
                            onColorSelected={this.submitColour}
                            style={{flex: 3}}
                        />
                        <View style={{flex: 1}}/>
                    </View>
                </View>
            </Modal>
        );
    }

    submitColour = colour => {
        this.updateSectionColour(colour);
        const colourVar = this.props.modalDetails.type === 'Font' ? 'textColour' : 'colour';
        this.props.showColourPickerAction({show: false, type: '', [colourVar]: colour});
        this.forceUpdate();
    };

    updateSectionColour = colour => {
        const {tierListArray, selectedTier, modalDetails} = this.props;
        const type = modalDetails.type === 'Font' ? 'sectionFontColour' : 'sectionColour';
        tierListArray.list[selectedTier][type] = colour;
    };

    cancelPress = () => {
        this.props.showColourPickerAction({show: false, type: ''});
        this.forceUpdate();
    };

  }

  const styles = StyleSheet.create({
    viewStyle: {
        backgroundColor: 'black',
        flex: 1
    },
    viewStyle2: {
        flex: 10,
        alignContent: 'center',
        justifyContent: 'center'
    },
    text: {
        textAlignVertical: 'center',
        fontSize: RFValue(25),
        color: 'white',
        textAlign: 'center',
    }
});

const mapStateToProps = (state) => {
    return{
      modalDetails: state.showModalReducer.showColourPicker,
      tierListArray: state.tierListReducer.tierListArray,
      selectedTier: state.selectedTierReducer.selectedTier
    }
};

export default connect(mapStateToProps, actions)(ColourPicker);