import React, {Component} from 'react';
import { Modal, TouchableOpacity, FlatList, StyleSheet, View, Alert, ScrollView, Image } from 'react-native';
import { Header } from "./Header";
import { RFValue } from "react-native-responsive-fontsize";
import {connect} from 'react-redux';
import * as actions from '../../actions';
import * as Permissions from 'expo-permissions';
import { ListButton } from './ListButton';
import CameraRoll from "@react-native-community/cameraroll";

class CameraModal extends Component {
    
    constructor(props) {
      super(props);
      this.state = {
        photos: [],
        lastCursor: null,
        load: true
      };
    };

    async componentDidUpdate(){
        if(this.props.modalVisible && this.state.load){
            const permission = await Permissions.getAsync(Permissions.CAMERA_ROLL);
          if (permission.status !== 'granted') {
              const newPermission = await Permissions.askAsync(Permissions.CAMERA_ROLL);
              if (newPermission.status === 'granted') {
                this._handleButtonPress();
              }
          } else {
            this._handleButtonPress();
          }
        }
    }

    render() {
      const {photos} = this.state;
      if(photos.length > 0){
        return (
          <Modal
            animationType="slide"
            transparent={false}
            visible={this.props.modalVisible}
            onDismiss={() => {
              this.setState({photos: [], lastCursor: null, load: true});
            }}>
            <View style={styles.viewStyle}>
              <Header headerText="Images" cancelOnPress={() => {this.props.showModalAction(false);this.forceUpdate();}} />
              <View style={styles.viewStyle2}>
              <ScrollView>
                <View style={{flex: 6}}>
                  <FlatList
                        numColumns={3}
                        data={photos}
                        renderItem={({ item }) => wrapData(item, this)}
                        keyExtractor={(item, index) => index.toString()}
                        />
                </View>
                <View style={{flex: 1, paddingTop: 50, paddingBottom: 50}}>
                  <ListButton btnText={"Load more"} onPress={() => {this.loadMore()}} />
                </View>
                      
              </ScrollView>
              
              </View>
          </View>
          </Modal>
        );
      }
      else{
        return(
          <View />
        )
      }
      
    }

    loadMore = () => {
      this._handleButtonPress();
    }

    _handleButtonPress = () => {

      const {lastCursor} = this.state;

      let options = {
        first: 51,
        assetType: 'Photos'
      }

      if(lastCursor){
        options.after = lastCursor;
      }

      CameraRoll.getPhotos(options)
        .then(r => {
          let arr = this.state.photos;
          const newArr = arr.concat(r.edges);
          this.setState({ photos: newArr, lastCursor: r.page_info.end_cursor, load: false });
        })
        .catch( err => {
            console.log(err);
        });
      };

  }

  wrapData = (image, CameraModal) => {
    if(image){
      const uri = image.node.image.uri;
      return (
          <TouchableOpacity
          key={uri}
          style={{flex:1/3, //here you can use flex:1 also
          aspectRatio:1}}
          onPress={() => {
            const count = CameraModal.props.selectedPhoto.photoCount++;
            CameraModal.props.photoChangedAction({uri, photoCount: count});
            CameraModal.props.hasPhotoChangedAction(true);
            CameraModal.props.showModalAction(false);
            CameraModal.forceUpdate();
          }}
          >
              <Image
                  resizeMode="cover"
                  key={uri}
                  style={{flex: 1}}
                  source={{ uri: uri}}
              />

          </TouchableOpacity>
      );  
    }  
}

  const styles = StyleSheet.create({
    viewStyle: {
        backgroundColor: 'black',
        flex: 1
    },
    viewStyle2: {
        flex: 10,
        alignContent: 'center',
        justifyContent: 'center'
    },
    text: {
        textAlignVertical: 'center',
        fontSize: RFValue(25),
        color: 'white',
        textAlign: 'center',
    }
});

mapStateToProps = (state) => {
    return{
      modalVisible: state.showModalReducer.showModal,
      selectedPhoto: state.selectedPhotoReducer.selectedPhoto
    }
}

export default connect(mapStateToProps, actions)(CameraModal);