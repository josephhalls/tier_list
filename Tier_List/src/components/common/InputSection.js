import React from 'react';
import {View, Text, TextInput} from 'react-native';
import { RFValue } from 'react-native-responsive-fontsize';
//const viewHeight = (Dimensions.get('window').height) / 10;

const InputSection = ({placeholder, value, onChangeText, label}) => {
    return(
        <View style={styles.viewStyles}>
            <View style={styles.textViewStyle}>
                <Text style={styles.labelStyles}>{label}:</Text>
            </View>
            <TextInput style={styles.inputStyles} placeholder={placeholder} value={value} onChangeText={onChangeText}/>
        </View>
    );
};

const styles = {
    viewStyles: {
        flexDirection: 'row',
        padding: 20,
        paddingBottom: 0,
        flex: 1
    },
    textViewStyle:{
        flex: 1,
        justifyContent: 'center',
    },
    labelStyles:{
        textAlign: 'left',
        fontSize: RFValue(15),
        color: 'black'
    },
    inputStyles:{
        flex: 2,
        color: 'black',
        borderWidth: 1,
        fontSize: RFValue(15),
        borderColor: 'black',
        padding: 10
    }
};

export {InputSection};