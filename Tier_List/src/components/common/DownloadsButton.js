import React from 'react';
import {View, Text, TouchableOpacity, Dimensions} from 'react-native';
import { RFValue } from 'react-native-responsive-fontsize';

const viewHeight = (Dimensions.get('window').height);

const DownloadsButton = ({btnText, onPress, isDownload}) => {
    return(
        <View>
            <TouchableOpacity style={[styles.viewStyle, isDownload ? styles.downloadNow : styles.noDownloadNeeded]} onPress={onPress}>
                <Text style={styles.textStyle}>{btnText}</Text>
            </TouchableOpacity>
        </View>
    );
};

const styles = {
    viewStyle: {
        borderWidth: 1,
        height: viewHeight / 10,
        borderColor: 'grey',
        padding: 20,
        paddingLeft: 30,
        paddingRight: 30,
        justifyContent: 'center',
        marginTop: 20,
        marginLeft: 20,
        marginRight: 20,
        backgroundColor: 'red'
    },
    noDownloadNeeded: {
        backgroundColor: 'grey'
    },
    downloadNow: {
        backgroundColor: 'red',
        borderColor: 'red'
    },
    
    textStyle: {
        color: 'white',
        textAlign: 'center',
        fontSize: RFValue(15)
    }
};

export {DownloadsButton};