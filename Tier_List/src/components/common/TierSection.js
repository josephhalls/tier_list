import React, {Component} from 'react';
import {View, Platform, Alert, AsyncStorage} from 'react-native';
import {CardSection, SectionHeader} from '../common';
import {connect} from 'react-redux';
import * as actions from '../../actions';
import ImageBox from '../common/ImageBox';
import {androidImagePicker, imageOrBlackSquare, modalDialog} from '../../core';
import Config from '../../../Config';
import CameraModal from './CameraModal';
import ColourPicker from './ColourPicker';


class TierSection extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showSectionDialogModal : false,
            showRNImageSelect : false
        };
    };

    componentDidUpdate(prevProps, prevStates, snapshot) {
        this.checkForDeletedImage(prevProps);
        this.newImage(prevProps);
    }

    newImage = (prevProps) => {
        if(prevProps.selectedPhoto !== this.props.selectedPhoto){
            this.addImage(this.props.selectedPhoto.uri);
            this.forceUpdate();
        }
    };

    render() {
        const {sectionText, colour, fontColour} = this.getSectionHeaderLetterColour();
        const {tierListArray, tierId, changeNameToggle} = this.props;
        const defaultText = tierListArray.list[tierId].sectionName ? tierListArray.list[tierId].sectionName : '';
        return (
            <CardSection key={tierId + '14'}>
                <SectionHeader
                    btnText={sectionText}
                    key={tierId + '19'}
                    colour={colour}
                    fontColour={fontColour}
                    colorbtnText={sectionText}
                    onPress={() => {
                        changeNameToggle ? this.editRow() : imageOrBlackSquare(this);
                    }}
                />
                <ColourPicker />
                {tierId === 0 ? <CameraModal /> : <View />}
                {this.ImageBoxes()}
                {this.state.showSectionDialogModal ? modalDialog('Rename your Tier Row', 'Please enter a name', defaultText, this.submitInput, this.state.showSectionDialogModal, this.closeDialog): <View/>}
            </CardSection>
        )
    }

    editRow = () => {
        this.props.tierChangedAction(this.props.tierId);
        Alert.alert(
            'Edit Tier Row',
            'Exit Edit Mode to apply your colour changes',
            [
                {
                    text: 'Change Name',
                    onPress: () => {
                        this.changeSectionName();
                    }
                },
                {
                    text: 'Change Background Colour',
                    onPress: () => {
                        this.changeSectionColour({show: true, type: 'Background'});
                    }
                },
                {
                    text: 'Change Font Colour',
                    onPress: () => {
                        this.changeSectionColour({show: true, type: 'Font'});
                    }
                },
                {
                    text: 'Cancel',
                }
            ]
        )
    };

    changeSectionName = () => {
        this.setState({showSectionDialogModal: true})
    };

    changeSectionColour = colourObject => {
        this.props.showColourPickerAction(colourObject);
    };

    submitInput = (inputText) => {
        const {tierListArray, tierId} = this.props;
        let array = tierListArray.list;
        array[tierId].sectionName = inputText;
        const listArr = {list: array};
        this.props.tierListAction(listArr);
        this.setState({showSectionDialogModal: false})
    };

    closeDialog = () => {this.setState({showSectionDialogModal: false})};

    getSectionHeaderLetterColour = () => {
        const {tierId, tierListArray} = this.props,
            alphabetArr = ['S', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'],
            colourArr = ['#fc3c2d', '#ff5042', '#ff7e42', '#ffcd4f', '#5fe371', '#79d9b7', '#6db58e', '#6da9f7', '#2380fa', '#9563ff', '#7352ba'];

        let index = tierId,
            colourArrLength = colourArr.length;

        if (index >= colourArrLength) {
            const count = Math.floor(tierId / colourArrLength);
            index = index - colourArrLength * count;
        }

        let colour = colourArr[index];

        if(this.props.changeNameToggle){
            colour = '#ffffff'
        }else if(tierListArray.list[tierId].sectionColour){
            colour = tierListArray.list[tierId].sectionColour
        }

        const sectionText = tierListArray.list[tierId].sectionName !== '' ? tierListArray.list[tierId].sectionName :alphabetArr[tierId];

        const customFont = tierListArray.list[tierId].sectionFontColour;

        const fontColour = customFont ? customFont : 'black';

        return {sectionText, colour, fontColour};
    };

    checkForDeletedImage = (prevProps) => {
        const {tierId, deletedImage, tierListArray, autoPadding} = this.props;
        if (prevProps.deletedImage !== this.props.deletedImage) {
            if (deletedImage[0] === tierId) {
                let newTierListArray = tierListArray.list;
                const singleTier = newTierListArray[tierId].images,
                    index = singleTier.indexOf(deletedImage[1]);
                if (index > -1) {
                    singleTier.splice(index, 1);
                    newTierListArray[tierId].images = singleTier;
                    if (autoPadding) {
                        newTierListArray = this.addBlackPadding(singleTier, newTierListArray, tierId);
                    }
                    const listArr = {list: newTierListArray};
                    this.props.tierListAction(listArr);
                    this.forceUpdate();
                }
            }
        }
    };

    addTierPressed = async () => {
        AsyncStorage.getItem('ImagePicker').then(imagePickerOption => {
            if(imagePickerOption && imagePickerOption !== 'none'){
                this.chooseImageSelector(imagePickerOption);
            }
            else{
                this.askUser();
            }
        })
    };

    chooseImageSelector = async (imagePickerOption) => {
        this.props.tierChangedAction(this.props.tierId);
        if(imagePickerOption === 'picker1'){
            const uri = await androidImagePicker();
            if(uri){
                this.addImage(uri);
                this.forceUpdate();
            }
        }
        else{
            await this.props.showModalAction(true);
            this.forceUpdate();
        }
    }

    askUser = (imagePickerOption) => {
        Alert.alert(
            'Please select an image picker',
            'You can save a default in the Options Menu. In case one image picker does not work with your device',
            [
                {
                    text: 'Image picker 1',
                    onPress: () => {
                        this.chooseImageSelector('picker1');
                    }
                },
                {
                    text: 'Image picker 2',
                    onPress: () => {
                        this.chooseImageSelector('picker2');
                    }
                },
                {
                    text: 'Cancel',
                }
            ]
        )
    }

    addImage = (image) => {
        const {autoPadding, tierId, tierListArray, selectedTier} = this.props;
        const listArr = tierListArray.list;
        if (autoPadding) {
            this.autoPaddingOn(listArr, image, tierId, selectedTier);
        } else {
            this.autoPaddingOff(listArr, tierId, image, selectedTier);
        }
    };

    autoPaddingOn = (tierListArray, image, tierId, selectedTier) => {
        let newTierListArray = tierListArray,
            singleTierArray = tierListArray[tierId].images,
            value = 'Black';

        let arr = singleTierArray.filter(item => item !== value);
        if(tierId === selectedTier){
            arr.push(image);
        }
        newTierListArray = this.addBlackPadding(arr, newTierListArray, tierId);
        const listArr = {list: newTierListArray};
        this.props.tierListAction(listArr);
    };

    autoPaddingOff = (tierListArray, tierId, image, selectedTier) => {
        const newTierListArray = tierListArray,
            singleTierArray = tierListArray[tierId].images;

        if(tierId === selectedTier){
            singleTierArray.push(image);
        }
        newTierListArray[tierId].images = singleTierArray;
        const listArr = {list: newTierListArray};
        this.props.tierListAction(listArr);
        this.forceUpdate();
    };

    addBlackPadding = (currentArr, tierListArray, tierId) => {
        const padding = currentArr.length >= Config.autoPadding ? false : (Config.autoPadding - currentArr.length);
        if(padding){
            for (let i = 0; i < padding; i++) {
                currentArr.push('Black');
            }
        }
        tierListArray[tierId].images = currentArr;
        return tierListArray;
    };

    ImageBoxes = () => {
        const {tierId, tierListArray} = this.props,
            singleTier = tierListArray.list[tierId].images,
            items = [];

        singleTier.filter((image, count) => {
            if(count === 0){
                return
            }
            const uniqueKey = image + count;
            items.push(<ImageBox key={uniqueKey} id={uniqueKey} uri={image} tierId={tierId}/>)
        });
        return (
            <View style={{flexDirection: 'row', flex: 6}}>
                {items}
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        selectedTier: state.selectedTierReducer.selectedTier,
        deletedImage: state.deletedImageReducer.deletedImage,
        saveDraft: state.saveDraftReducer.saveDraftArray,
        autoPadding: state.autoPaddingReducer.autoPadding,
        tierListArray: state.tierListReducer.tierListArray,
        changeNameToggle: state.changeNameToggleReducer.changeNameToggle,
        selectedPhoto: state.selectedPhotoReducer.selectedPhoto
    }
};

export default connect(mapStateToProps, actions)(TierSection);