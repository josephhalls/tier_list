import React, {Component} from 'react';
import {View, Dimensions, Image, Alert, TouchableOpacity} from 'react-native';
const borderHeight = 5;
//const viewHeight = (Dimensions.get('window').height) / 8 - borderHeight;
const viewWidth = (Dimensions.get('window').height) / 10;
import {connect} from 'react-redux';
import * as actions from '../../actions';

class ImageBox extends Component{
    
    render(){
        const {uri, id} = this.props;
        return(
            <View key={id} style={styles.containerStyle}>
                <TouchableOpacity
                    key={id}
                    style={{flex:1}}
                    onPress={() => {this.imagePressed();}}
                    >
                    {!uri.includes('Black') ? this.imageView(uri, id) : <View key={id} />}
                    
                </TouchableOpacity>
            </View>
        )
    }

    imageView = (uri, id) =>{
        return(
            <Image
                resizeMode={this.props.imageStyle}
                key={id}
                style={{flex: 1}}
                source={{ uri: uri}}
                />
        )
    };

    imagePressed = () => {
        const {tierId, uri} = this.props;
        Alert.alert(
            'Delete Image',
            'Would you like to delete this image?',
            [
              {
                  text: 'Yes', 
                  onPress: () =>  this.props.deletedImageAction([(tierId), uri])
              },
              {text: 'No'}
            ],
            {cancelable: false},
          );
    }
}

const styles = {
    containerStyle: {
        //height: viewHeight,
        flex: 1,
        width: viewWidth,
        marginRight: 10,
        backgroundColor: 'black'
    }
};

const mapStateToProps = (state) => {
    return{
        imageStyle: state.imageStyleReducer.imageStyle
    }
};

export default connect(mapStateToProps, actions)(ImageBox);