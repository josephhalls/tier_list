import React from 'react';
import {Text, TouchableOpacity} from 'react-native';
import { RFValue } from 'react-native-responsive-fontsize';
import Config from '../../../Config';

//const viewHeight = (Dimensions.get('window').height);

const OptionMenuButton = ({btnText, onPress}) => {
    return(
        <TouchableOpacity style={styles.viewStyle} onPress={onPress}>
            <Text style={styles.textStyle}>{btnText}</Text>
        </TouchableOpacity>
    );
};

const styles = {
    viewStyle: {
        flex: 1,
        paddingLeft: 30,
        paddingRight: 30,
        justifyContent: 'center',
        marginLeft: 20,
        marginRight: 22,
        backgroundColor: 'transparent'
    },
    textStyle: {
        color: 'white',
        textAlign: 'center',
        fontSize: RFValue(20),
        fontFamily: Config.fontFamily
    },
};

export {OptionMenuButton};