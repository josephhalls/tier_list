import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import { RFValue } from 'react-native-responsive-fontsize';
import Config from '../../../Config';

class DraftSection extends Component {
    render(){
        const {draftName, onPress, closeOnPress, i} = this.props;
        return(
            <View key={i + 8} style={styles.viewStyling}>
                <TouchableOpacity key={i + 1} style={{flex: 8}} onPress={onPress}>
                    <Text key={i+ 2} style={styles.titleText}>{draftName}</Text>
                </TouchableOpacity>
                <TouchableOpacity key={i + 3} style={{flex: 1, backgroundColor: 'black'}} onPress={closeOnPress}>
                    <Text key={i + 4} style={styles.titleText}>x</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    viewStyle: {
        backgroundColor: 'black',
        flex: 1
    },
    viewStyling: {
        flexDirection: 'row',
        flex: 1,
        borderWidth: 1,
        borderColor: 'black',
        padding: 16,
        paddingLeft: 30,
        paddingRight: 30,
        justifyContent: 'center',
        marginTop: 20,
        marginLeft: 20,
        marginRight: 22,
        backgroundColor: 'black'
    },
    viewStyle2: {
        flex: 10,
        alignContent: 'center',
        justifyContent: 'center'
    },
    titleText: {
        textAlignVertical: 'center',
        fontSize: RFValue(30),
        color: 'white',
        textAlign: 'center',
        flex: 1
    },
    text: {
        textAlignVertical: 'center',
        fontSize: RFValue(20),
        fontFamily: Config.fontFamily,
        color: 'white',
        textAlign: 'center',
        flex: 1
    }
});

export default DraftSection;