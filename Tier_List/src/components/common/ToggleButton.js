import React from 'react';
import {Text, TouchableOpacity, Dimensions} from 'react-native';

const viewHeight = (Dimensions.get('window').height) / 10;

const ToggleButton = ( {onPress, btnText, toggleValue}) => {
    return(
        <TouchableOpacity style={[styles.buttonStyle, toggleValue ? {backgroundColor: 'grey'} : {backgroundColor: '#fff'}]} onPress={onPress}>
            <Text style={[styles.textStyle, toggleValue ? {color: 'white'} : {color: 'black'}]}>{btnText}</Text>
        </TouchableOpacity>
    );
};

const styles = {
    buttonStyle: {
        textAlign: 'center',
        fontSize: 18,
        height: viewHeight,
        flex: 1,
        alignSelf: 'stretch',
        backgroundColor: '#fff',
        borderWidth: 1,
        borderColor: 'black',
        marginTop: 15,
        justifyContent: 'center'
    },
    textStyle: {
        alignSelf: 'center',
        color: 'black',
        fontSize: 16,
        fontWeight: '600',
        paddingTop: 10,
        paddingBottom: 10
    },
    toggleNotSelected: {
        
    }

};

export { ToggleButton };