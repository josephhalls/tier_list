import React from 'react';
import {View, Text, TextInput, Dimensions} from 'react-native';
import { RFValue } from 'react-native-responsive-fontsize';
const viewHeight = (Dimensions.get('window').height) / 5;

const LargeInputSection = ({label, placeholder, onChangeText, value}) => {
    const {viewStyles, textViewStyle, labelStyles, inputStyles} = styles;
    return(
        <View style={viewStyles}>
            <View style={textViewStyle}>
                <Text style={labelStyles}>{label}:</Text>
            </View>
            <TextInput multiline = {true} numberOfLines = {4} style={inputStyles} placeholder={placeholder} onChangeText={onChangeText}>{value}</TextInput>
        </View>
    );
};

const styles = {
    viewStyles: {
        flexDirection: 'row',
        padding: 20,
        paddingBottom: 0,
        height: viewHeight
    },
    textViewStyle:{
        flex: 1,
        justifyContent: 'center',
    },
    labelStyles:{
        textAlign: 'left',
        fontSize: RFValue(15),
        color: 'black'
    },
    inputStyles:{
        flex: 2,
        color: 'black',
        borderWidth: 1,
        fontSize: RFValue(15),
        borderColor: 'black',
        padding: 10
    }
};

export {LargeInputSection};