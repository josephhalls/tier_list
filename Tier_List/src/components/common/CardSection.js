import React from 'react';
import {View} from 'react-native';

//const viewHeight = (Dimensions.get('window').height) / 8;

const CardSection = (props) => {
    return (
        <View style={styles.containerStyle}>
            {props.children}
        </View>
    );
};

//To switch between set height and flex, play with the height an flex here
const styles = {
    containerStyle: {
        backgroundColor: 'black',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        position: 'relative',
        flex: 1,
        //height: viewHeight
    }
};

export {CardSection};