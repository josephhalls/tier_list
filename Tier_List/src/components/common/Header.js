import React from 'react';
import {View, Text, StatusBar, Image, TouchableOpacity} from 'react-native';
import { RFValue } from 'react-native-responsive-fontsize';
import {Actions} from 'react-native-router-flux';
import Config from '../../../Config';

const Header = ({headerText, rightText, onPress, cancelOnPress, backButtonHidden, listPage, downloadOnPress, settingsOnPress}) => {

    const {viewStyle, mainTitleStyle, headerButtonText} = styles;
    
    return (
        <View style={viewStyle}>
            <View style={{flex: 1}}>
                {backButton(headerButtonText, backButtonHidden, cancelOnPress)}
            </View>

            <Text style={mainTitleStyle}>{headerText}</Text>

            <View style={{flex: 1, fontFamily: Config.fontFamily}}>
                {rightText ? saveWrap(onPress, rightText) : listPage ? touchableImage(downloadOnPress, settingsOnPress) : <View />}
            </View>
        </View>
    );
};

const backButton = (headerButtonText, backButtonHidden, cancelOnPress) => {
    if(!backButtonHidden){
        return(
            <TouchableOpacity onPress={cancelOnPress ? cancelOnPress : () => Actions.pop() }>
                <Text style={headerButtonText}>Back</Text>
            </TouchableOpacity>
        );
    }
};

const touchableImage = (downloadOnPress, settingsOnPress) => {
    return(
        <View style={{flex: 1}}>
            <View style={{flex: 1}} />
            <View style={{flexDirection: 'row', flex: 3, alignItems: 'center', justifyContent: 'center'}}>
                <TouchableOpacity style={{flex: 1, alignContent: 'center'}} onPress={downloadOnPress}>
                    <Image
                        resizeMode='contain'
                        style={{flex: 1, width: '60%', height: '50%', alignItems: 'center', justifyContent: 'center'}}
                        source={require('../../../assets/download_icon.png')}
                    />
                </TouchableOpacity>
                <TouchableOpacity style={{flex: 1}} onPress={settingsOnPress}>
                    <Image
                        resizeMode='contain'
                        style={{flex: 1, alignItems: 'center', width: '60%', height: '50%', justifyContent: 'center'}}
                        source={require('../../../assets/settings_icon.png')}
                    />.
                </TouchableOpacity>
            </View>
            <View style={{flex: 1}} />
        </View>
    );
};

const saveWrap = (onPress, rightText) => {
    const {headerButtonText} = styles;
    return(
        <TouchableOpacity onPress={onPress}>
            <Text style={headerButtonText}>{rightText}</Text>
        </TouchableOpacity>
    );
};

StatusBar.setBarStyle('light-content', true);

const styles = {
    viewStyle: {
        textAlign: 'center',
        flexDirection: 'row',
        backgroundColor: 'black',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        paddingTop: 35,
        shadowColor: '#000',
        shadowOpacity: 0.9,
        elevation: 20,
        position: 'relative'
    },
    mainTitleStyle: {
        textAlign: 'center',
        flex: 2,
        fontSize: RFValue(25),
        color: 'white',
        fontFamily: Config.fontFamily
    },
    headerButtonText: {
        textAlign: 'center',
        fontSize: RFValue(15),
        color: 'white',
        fontFamily: Config.fontFamily
    }
};

export {Header};