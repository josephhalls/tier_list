import React from 'react';
import {Dimensions, Text, TouchableOpacity} from 'react-native';

//const borderHeight = 5;
//const viewHeight = (Dimensions.get('window').height) / 8 - borderHeight;
const viewWidth = (Dimensions.get('window').height) / 12;

const SectionHeader = ({btnText, onPress, colour, fontColour, layout}) => {
    return (
        <TouchableOpacity style={[styles.containerStyle, {backgroundColor: colour, zIndex: 9999}]} onPress={onPress}>
            <Text style={[[styles.textStyle, {color: [fontColour]}]]}>{btnText}</Text>
        </TouchableOpacity>
    );
};

const styles = {
    containerStyle: {
        //height: viewHeight,
        flex: 1,
        width: viewWidth,
        marginRight: 5,
        backgroundColor: 'red',
        justifyContent: 'center'
    },
    textStyle: {
        color: 'black',
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'center',
        textAlignVertical: 'center',
    }
};

export {SectionHeader};