import React from 'react';
import {View} from 'react-native';
import Modal from 'react-native-modal';
import {OptionMenuButton} from '../components/common';
import {imageStyleModal} from '../core';

export const menuModal = (self, newTierList) => {
    const orientationText = self.state.DeviceOrientation === 'PORTRAIT' ? 'Horizontal View' : 'Portrait View';
    return(
        <View style={{flex: 1}}>
            <Modal isVisible={self.state.menuModal} style={{flex: 1}}>
                <View style={{ flex: 1, backgroundColor: 'black' }}>
                    <OptionMenuButton btnText='New Tier List' onPress={() => newTierList()} />
                    <OptionMenuButton btnText='Image Picker' onPress={() => self.imagePickerOption()} />
                    <OptionMenuButton btnText={'Auto padding' + ' ' + self.state.autoPaddingText} onPress={() => self.autoPadding()} />
                    <OptionMenuButton btnText='Save Draft' onPress={() => self.saveDraft()} />
                    <OptionMenuButton btnText='Update Image style' onPress={() => imageStyleModal(self)} />
                    <OptionMenuButton btnText='Download Screenshot' onPress={() => self.saveScreenPermission()}/>
                    <OptionMenuButton btnText={orientationText} onPress={() => self.changeScreenOrientation()}/>
                    <OptionMenuButton btnText='Close' onPress={() => self.closeMenuModal()}/>
                </View>
            </Modal>
        </View>
    );
};