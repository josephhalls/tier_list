import CheckPremium from './CheckPremium';
import {Platform} from 'react-native';

const fontFamily = Platform.OS === 'ios' ? 'symbol' : 'Roboto';

export default Config = {
    isPremium : false,
    tierLevels: 6,
    autoPadding: 5,
    fontFamily: Platform.OS === 'ios' ? 'symbol' : 'Roboto',
    android_home_admob_key: Platform.OS === 'ios' ? 'ca-app-pub-8451457663728486/2303997810' : 'ca-app-pub-8451457663728486/5413690236',
    android_list_home_admob_key: Platform.OS === 'ios' ? 'ca-app-pub-8451457663728486/7425582119' : 'ca-app-pub-8451457663728486/2409216970',//
    android_instructions_home_admob_key: Platform.OS === 'ios' ? 'ca-app-pub-8451457663728486/5413690236' : 'ca-app-pub-8451457663728486/3029921976',
    android_upgrade_admob_key: Platform.OS === 'ios' ? 'ca-app-pub-8451457663728486/3029921976' : 'ca-app-pub-8451457663728486/7425582119',
    android_contact_admob_key: Platform.OS === 'ios' ? 'ca-app-pub-8451457663728486/2409216970' : 'ca-app-pub-8451457663728486/2303997810',
    android_video_admob_key: Platform.OS === 'ios' ? 'ca-app-pub-8451457663728486/9721492832' : 'ca-app-pub-8451457663728486/9721492832'
}
