import React, {Component} from 'react';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import ReduxThunk from 'redux-thunk';
import reducers from './src/reducers';
import Router from './src/Router';
import Loading from './src/activities/Loading';
import {View, ActivityIndicator} from 'react-native';

class App extends Component{

    constructor(props) {
        super(props);
        this.state = {
            isLoaded: false
        };
    }

    componentDidMount(){
        Loading(this, () => {
            this.setState({isLoaded : true});
        });
    }

    render(){
        const {isLoaded} = this.state;
        const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));

        if(isLoaded){
            return(
                <Provider store={store}>
                    <Router/>
                </Provider>
            );
        }

        return(
            <View style={{flex: 1, justifyContent: 'center'}}>
                <ActivityIndicator size='large' color='#000000' />
            </View>
        );
    }
}

export default App;
